<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Tests;

use \Selene\Adapter\Kernel\StackBuilder;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @class StackBuilderTest extends StackedKernelTest
 * @see StackedKernelTest
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class StackBuilderTest extends StackedKernelTest
{
    /** @test */
    public function itShouldBeInstantiable()
    {
        $this->assertInstanceof('Selene\Module\Stack\StackBuilder', new StackBuilder($this->mockKernel()));
    }

    /** @test */
    public function middlewaresShouldBeAddable()
    {
        $builder = new StackBuilder($kernel = $this->mockKernel());

        $midA    =  $this->mockStackedKernel();
        $midB    =  $this->mockStackedKernel();

        $midA->shouldReceive('getPriority')->andReturn(10);
        $midB->shouldReceive('getPriority')->andReturn(0);

        $midA->shouldReceive('setKernel')->with($kernel);
        $midA->shouldReceive('getKernel')->andReturn($kernel);
        $midB->shouldReceive('setKernel')->with($midA);
        $midB->shouldReceive('getKernel')->andReturn($midA);

        $builder->add($midA);
        $builder->add($midB);

        $stack = $builder->make();

        $this->assertInstanceof('Selene\Module\Stack\Stack', $stack);
        $this->assertInstanceof('Symfony\Component\HttpKernel\HttpKernelInterface', $stack);
    }
}
