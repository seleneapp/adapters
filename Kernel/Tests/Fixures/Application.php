<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Tests\Fixures;

use \Selene\Module\DI\ContainerInterface;
use \Selene\Adapter\Kernel\Application as App;

/**
 * @class Application extends App
 * @see App
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Application extends App
{
    /**
     * {@inheritdoc}
     */
    public function getApplicationServiceId()
    {
        return 'app';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestStack()
    {
        return $this->getContainer()->get('request.stack');
    }

    /**
     * {@inheritdoc}
     */
    public function getKernelStack()
    {
        return $this->getContainer()->get($this->getKernelServiceId().'.stack');
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerServiceId()
    {
        return $this->getApplicationServiceId().'.container';
    }

    /**
     * {@inheritdoc}
     */
    public function getKernelServiceId()
    {
        return $this->getApplicationServiceId().'.kernel';
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultParameters()
    {
        $appid = $this->getApplicationServiceId();
        $kerid = $this->getKernelServiceId();

        return array_merge(
            $this->getPackageInfo($appid),
            [
                $kerid.'.root'      => $this->getApplicationRoot(),
                $appid.'.root'      => $this->getApplicationRoot(),
                $appid.'.env'       => $this->getEnvironment(),
                $appid.'.debugging' => null !== $this->debugger,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function containerPreBuild(ContainerInterface $container, $containerClass, $containerFile)
    {
        $container->setParameter($this->getContainerServiceId().'.class', $containerClass);
        $container->setParameter($this->getContainerServiceId().'.file', $containerFile);
        $container->setParameter($this->getApplicationServiceId().'.class', get_class($this));
        $container->setParameter($this->getApplicationServiceId().'.package.sources', $this->packageResources);
    }

    /**
     * Inject services to the container.
     *
     * @return void
     */
    protected function injectServicesOnBoot()
    {
        $container = $this->getContainer();

        // injecting the package repository
        $container->inject($this->getApplicationServiceId().'.package.repository', $this->packages);

        // injecting the kernel stack:
        $container->inject(
            $this->getKernelServiceId().'.stack',
            $container->get($this->getKernelServiceId().'.stackbuilder')
        );
    }
}
