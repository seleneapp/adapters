<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Tests;

use \Selene\Adapter\Kernel\Stack;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @class StackTest extends StackedKernelTest
 * @see StackedKernelTest
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class StackTest extends StackedKernelTest
{
    /** @test */
    public function itShouldBeInstantiable()
    {
        $this->assertInstanceof('Selene\Module\Stack\Stack', new Stack($this->mockKernel()));
        $this->assertInstanceof('Selene\Module\Stack\Stack', new Stack($this->mockStackedKernel()));
    }

    /** @test */
    public function itShouldCallHandleOnItsParentKenel()
    {
        $stack = new Stack($kernel = $this->mockStackedKernel());

        $request = Request::create('/');

        $kernel->shouldReceive('handle')
            ->with($request, HttpKernelInterface::MASTER_REQUEST, true)
            ->andReturn(new Response('success'));

        $response = $stack->handle($request);
        $this->assertSame('success', $response->getContent());
    }

    /** @test */
    public function itShouldCallTerminateOnItsParentKenel()
    {
        $stack = new Stack($kernel = $this->mockTerminableKernel());

        $request = Request::create('/');

        $response = new Response('error');

        $kernel->shouldReceive('terminate')
            ->with($request, $resp = m::mock('Symfony\Component\HttpFoundation\Response'))
            ->andReturnUsing(function ($req, $res) use ($response) {
                $response->setContent('success');
            });

        $stack->terminate($request, $resp);
        $this->assertSame('success', $response->getContent());
    }
}
