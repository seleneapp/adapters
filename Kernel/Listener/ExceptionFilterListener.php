<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Listener package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Listener;

use \Selene\Module\Events\EventInterface;
use \Selene\Module\Events\EventListenerInterface;
use \Selene\Adapter\Kernel\Filter\ExceptionFilter;
use \Selene\Adapter\Kernel\Event\ExceptionEventInterface;
use \Selene\Adapter\Kernel\Filter\ExceptionFilterInterface;

/**
 * @class ExceptionFilterListener
 *
 * @package Selene\Adapter\Kernel\Listener
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class ExceptionFilterListener implements EventListenerInterface
{
    /**
     * Constructor.
     *
     * @param ExceptionFilterInterface $filter
     */
    public function __construct(ExceptionFilterInterface $filter = null)
    {
        $this->filter = $filter ?: new ExceptionFilter;
    }

    /**
     * handleEvent
     *
     * @param EventInterface $event
     * @throws \InvalidArgumentException
     *
     * @return void
     */
    public function handleEvent(EventInterface $event)
    {
        if (!($event instanceof ExceptionEventInterface)) {
            throw $this->invalidEventType($event);
        }

        $this->filter->setException($event->getException());

        if (!$response = $event->getResponse()) {
            $response = new Response($event->getException()->getMessage(), 500);
            $event->setResponse($response);
        }

        $this->filter->filter($response);
    }

    /**
     * invalidEventType
     *
     * @param EventInterface $event
     *
     * @return \InvalidArgumentException
     */
    private function invalidEventType(EventInterface $event)
    {
        return new \InvalidArgumentException(sprintf(
            'Event must be type of "ExceptionEventInterface", instead saw "%s".',
            get_class($event)
        ));
    }
}
