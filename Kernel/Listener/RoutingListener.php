<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Listener package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Listener;

use \Selene\Module\Events\EventInterface;
use \Selene\Module\Routing\RouterInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Selene\Module\Events\EventListenerInterface;
use \Selene\Adapter\Kernel\Event\KernelEventInterface;

/**
 * @class RoutingListener
 *
 * @package Selene\Adapter\Kernel\Listener
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class RoutingListener implements EventListenerInterface
{
    private $router;

    /**
     * Constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * handleEvent
     *
     * @param EventInterface $event
     *
     * @return void
     */
    public function handleEvent(EventInterface $event)
    {
        if (!($event instanceof KernelEventInterface)) {
            throw $this->invalidEventType($event);
        }

        if (null === ($response = $this->router->dispatch($event->getRequest(), $event->getRequestType()))) {
            return;
        }

        $event->setResponse($response = $this->getResponse($response));
        $event->stopPropagation();
    }

    /**
     * getResponse
     *
     * @param mixed $result
     *
     * @return Response
     */
    private function getResponse($result)
    {
        if ($result instanceof Response) {
            return $result;
        }

        return new Response($result);
    }

    private function invalidEventType(EventInterface $event)
    {
        return new \InvalidArgumentException(sprintf(
            'Event must by type of "Selene\Adapter\Kernel\Event\KernelEventInterface", instead saw %s',
            get_class($event)
        ));
    }
}
