<?php

/**
 * This File is part of the Selene\Module\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Subscriber;

use \Selene\Module\Routing\RouterInterface;
use \Selene\Module\Events\SubscriberInterface;
use \Selene\Module\Events\Traits\SubscriberTrait;
use \Selene\Adapter\Kernel\Event\KernelEvents as Events;
use \Selene\Adapter\Kernel\Event\ControllerEventInterface;
use \Symfony\Component\HttpFoundation\Response;

/**
 * Subscriber to the kernel::DISPATCH event.
 *
 * Will dispatch the router with the given request.
 *
 * @see SubscriberInterface
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class RouterSubscriber implements SubscriberInterface
{
    use SubscriberTrait;

    /**
     * router
     *
     * @var RouterInterface
     */
    private $router;

    /**
     * subscriptions
     *
     * @var array
     */
    private static $subscriptions = [
        Events::DISPATCH => 'onDispatchRequest'
    ];

    /**
     * Constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * onHandleRequest
     *
     * @param ControllerEventInterface $event
     *
     * @return void
     */
    public function onDispatchRequest(ControllerEventInterface $event)
    {
        if (null === ($response = $this->router->dispatch($event->getRequest(), $event->getRequestType()))) {
            return;
        }

        $event->setResponse($response = $this->getResponse($response));
        $event->stopPropagation();
    }

    /**
     * getResponse
     *
     * @param mixed $result
     *
     * @return Response
     */
    private function getResponse($result)
    {
        if ($result instanceof Response) {
            return $result;
        }

        return new Response($result);
    }
}
