<?php

/**
 * This File is part of the Selene\Module\Kernel\Subscriber package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Subscriber;

use \Selene\Module\Events\FilterResponseEvent;
use \Selene\Module\Events\SubscriberInterface;
use \Selene\Adapter\Kernel\Event\KernelEvents as Events;

/**
 * @class ResponseSubscriber
 * @package Selene\Module\Kernel\Subscriber
 * @version $Id$
 */
class ResponseFilterSubsciber implements SubscriberInterface
{
    /**
     * onFilterResponse
     *
     * @param FilterResponseEvent $event
     *
     * @return void
     */
    public function onFilterResponse(FilterResponseEvent $event)
    {
        $event->setResponse($this->filterResponse($event->getRequest(), $event->getResponse()));
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscriptions()
    {
        return [
            Events::FILTER_RESPONSE => 'onFilterResponse'
        ];
    }

    /**
     * filterResponse
     *
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    private function filterResponse(Request $request, Response $response)
    {
    }
}
