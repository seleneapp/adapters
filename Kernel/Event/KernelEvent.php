<?php

/**
 * This File is part of the Selene\Module\Kernel\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

use \Selene\Module\Events\Event;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @class KernelEvent
 * @see Event
 * @abstract
 *
 * @package Selene\Adapter\Kernel\Event
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
abstract class KernelEvent extends Event implements KernelEventInterface
{
    /**
     * kernel
     *
     * @var mixed
     */
    private $kernel;

    /**
     * request
     *
     * @var Request
     */
    private $request;

    /**
     * requestType
     *
     * @var int
     */
    private $requestType;

    /**
     * Constructor.
     *
     * @param HttpKernelInterface $kernel
     * @param Request             $request
     * @param int                 $type
     */
    public function __construct(HttpKernelInterface $kernel, Request $request, $type)
    {
        $this->kernel = $kernel;
        $this->request = $request;
        $this->requestType = $type;
    }

    /**
     * getRequest
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * getKernel
     *
     * @return HttpKernelInterface
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * getRequestType
     *
     * @return int
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * isMasterRequest
     *
     * @return Boolean
     */
    public function isMasterRequest()
    {
        HttpKernelInterface::MASTER_REQUEST === $this->getRequestType();
    }

    /**
     * isSubRequest
     *
     * @return Boolean
     */
    public function isSubRequest()
    {
        HttpKernelInterface::SUB_REQUEST === $this->getRequestType();
    }
}
