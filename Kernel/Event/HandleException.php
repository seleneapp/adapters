<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @class KernelEvent
 * @package Selene\Module\Kernel
 * @version $Id$
 */
class HandleException extends GetResponse implements ExceptionEventInterface
{
    /**
     * exception
     *
     * @var \Exception
     */
    private $exception;

    /**
     * Constructor.
     *
     * @param HttpKernelInterface $kernel
     * @param Request $request
     * @param mixed $type
     * @param \Exception $exception
     */
    public function __construct(HttpKernelInterface $kernel, Request $request, $type, \Exception $exception)
    {
        $this->exception = $exception;

        parent::__construct($kernel, $request, $type);

    }

    /**
     * getException
     *
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }
}
