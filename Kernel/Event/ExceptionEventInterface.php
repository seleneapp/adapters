<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Event package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

/**
 * @class ExceptionEventInterface
 *
 * @package Selene\Adapter\Kernel\Event
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface ExceptionEventInterface extends ResponseEventInterface
{
    /**
     * getException
     *
     * @return \Exception
     */
    public function getException();
}
