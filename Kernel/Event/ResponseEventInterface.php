<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Event package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

use \Symfony\Component\HttpFoundation\Response;

/**
 * @interface ResponseEvent
 *
 * @package Selene\Adapter\Kernel\Event
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface ResponseEventInterface extends KernelEventInterface
{
    /**
     * setResponse
     *
     * @param Response $response
     *
     * @return void
     */
    public function setResponse(Response $response);

    /**
     * getResponse
     *
     * @return Response|null
     */
    public function getResponse();

    /**
     * hasResponse
     *
     * @return boolean
     */
    public function hasResponse();
}
