<?php

/**
 * This File is part of the \Users\malcolm\www\selene_source\src\Selene\Module\Kernel\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

use \Symfony\Component\HttpFoundation\Response;

/**
 * @class HandleRequestEvent
 * @package \Users\malcolm\www\selene_source\src\Selene\Module\Kernel\Events
 * @version $Id$
 */
class HandleRequest extends GetResponse implements ControllerEventInterface
{
}
