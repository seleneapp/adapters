<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

use \Symfony\Component\HttpFoundation\Response;

/**
 * @class ResponseEvent extends KernelEvent
 * @see KernelEvent
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class GetResponse extends KernelEvent implements ResponseEventInterface
{
    /**
     * response
     *
     * @var Response
     */
    private $response;

    /**
     * {@inheritdoc}
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * {@inheritdoc}
     */
    public function hasResponse()
    {
        return null !== $this->getResponse();
    }
}
