<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

/**
 * @class AbortRequestEvent
 * @package Selene\Module\Kernel\Events
 * @version $Id$
 */
/**
 * @class AbortRequestEvent extends HandleExceptionEvent
 * @see HandleExceptionEvent
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class AbortRequestEvent extends HandleException
{
}
