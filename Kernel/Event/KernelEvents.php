<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

/**
 * @final class KernelEvents
 * @final
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
final class KernelEvents
{
    /**
     * Event dispatched just before the kernel starts to dispatch the request.
     */
    const REQUEST       = 'kernel.request';

    /**
     * Event when the request is dispatched.
     */
    const DISPATCH      = 'kernel.request.dispatch';

    /**
     *
     */
    const END_REQUEST   = 'kernel.request_end';

    /**
     *
     */
    const RESPONSE      = 'kernel.response';

    /**
     *
     */
    const FILTER_RESPONSE_BEFORE = 'kernel.response.filter_before';

    /**
     *
     */
    const FILTER_RESPONSE_AFTER  = 'kernel.response.filter_after';

    /**
     *
     */
    const HANDLE_RESPONSE  = 'kernel.handle_response';

    /**
     *
     */
    const EXCEPTION = 'kernel.exception';

    /**
     *
     */
    const TERMINATE  = 'kernel.terminate';

    /**
     *
     */
    const ABORT_REQUEST    = 'kernel.abort_request';

    private function __construct()
    {
    }
}
