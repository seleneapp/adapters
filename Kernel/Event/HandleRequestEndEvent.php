<?php

/**
 * This File is part of the Selene\Module\Kernel\Events package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Event;

/**
 * @class HandleRequestEndEvent
 * @package Selene\Module\Kernel\Events
 * @version $Id$
 */
class HandleRequestEndEvent extends KernelEvent
{
}
