<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel;

use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @interface StackedKernelInterface extends HttpKernelInterface
 * @see HttpKernelInterface
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
interface StackedKernelInterface extends HttpKernelInterface
{
    /**
     * setKernel
     *
     * @param HttpKernelInterface $kernel
     *
     * @return void
     */
    public function setKernel(HttpKernelInterface $kernel);

    /**
     * getKernel
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface;
     */
    public function getKernel();

    /**
     * get the priority on the kernel stack
     *
     * @return integer
     */
    public function getPriority();
}
