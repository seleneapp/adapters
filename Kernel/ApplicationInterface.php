<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @interface ApplicationInterface extends HttpKernelInterface
 * @see HttpKernelInterface
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
interface ApplicationInterface extends HttpKernelInterface
{
    /**
     * Boot the application
     *
     * @return void
     */
    public function boot();

    /**
     * Starts the application
     *
     * @return void
     */
    public function run(Request $request = null);

    /**
     * getLoadedPackages
     *
     * @return array
     */
    public function getPackages();

    /**
     * Get application version.
     *
     * @return string
     */
    public static function version();
}
