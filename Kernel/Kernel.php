<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\HttpKernelInterface;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Symfony\Component\HttpKernel\TerminableInterface;
use \Selene\Adapter\Http\RequestStack;
use \Selene\Adapter\Kernel\Filter\ExceptionFilter;
use \Selene\Adapter\Kernel\Event\AbortRequestEvent;
use \Selene\Adapter\Kernel\Event\HandleShutDownEvent;
use \Selene\Adapter\Kernel\Event\HandleRequestEndEvent;
use \Selene\Adapter\Kernel\Event\GetResponse;
use \Selene\Adapter\Kernel\Event\HandleRequest;
use \Selene\Adapter\Kernel\Event\FilterResponse;
use \Selene\Adapter\Kernel\Event\HandleException;
use \Selene\Adapter\Kernel\Event\KernelEvents as Events;
use \Selene\Module\Events\DispatcherInterface;
use \Selene\Module\Events\SubscriberInterface;

/**
 * @class Kernel implements HttpKernelInterface
 * @see HttpKernelInterface
 *
 * @package Selene\Module\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Kernel implements KernelInterface, TerminableInterface
{
    /**
     * events
     *
     * @var mixed
     */
    private $events;

    /**
     * responseStack
     *
     * @var mixed
     */
    protected $responseStack;

    /**
     * requestStack
     *
     * @var mixed
     */
    protected $requestStack;

    /**
     * Create a new Kernel instance.
     *
     * @param DispatcherInterface $events
     * @param RequestStack $stack
     */
    public function __construct(DispatcherInterface $events, RequestStack $stack = null)
    {
        $this->events = $events;
        $this->requestStack = $stack ?: new RequestStack;
    }

    /**
     * handle
     *
     * @param Request $request
     * @param string  $type
     * @param int     $catch
     *
     * @throws \Exception if $catch is false and program raised exception
     * during request handling.
     *
     * @return Response
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $this->requestStack->push($request);

        try {
            $response = $this->handleRequest($request, $type, $catch);
        } catch (\Exception $e) {
            if (!$catch) {
                $this->endRequest($request, $type);

                throw $e;
            }

            return $this->handleRequestException($request, $e, $type);
        }

        return $response;
    }

    /**
     * terminate
     *
     * @param Request $request
     * @param Response $response
     *
     * @access public
     * @return mixed
     */
    public function terminate(Request $request, Response $response)
    {
        //Fire finishing events
        $this->getEvents()->dispatch(
            Events::TERMINATE,
            $event = new HandleShutDownEvent($request, $response)
        );
    }

    /**
     * getEvents
     *
     *
     * @access public
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * registerKernelSubscriber
     *
     * @param SubscriberInterface $subscriber
     *
     * @access public
     * @return void
     */
    public function registerKernelSubscriber(SubscriberInterface $subscriber)
    {
        $this->getEvents()->addSubscriber($subscriber);
    }

    /**
     * handleRequest
     *
     * @param Request $request
     * @param int     $type
     * @param boolean $catch
     *
     * @return Resoonse
     */
    protected function handleRequest(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $event = $this->fireRequestEvent($request, $type);

        if ($event->hasResponse()) {
            return $this->filterResponse($request, $event->getResponse(), $type, Events::FILTER_RESPONSE_BEFORE);
        }

        //Fire the first kernel event in order to retreive an response. If no
        //response is set on the event, return a 404 response.
        $event = new HandleRequest($this, $request, $type);
        $this->events->dispatch(Events::DISPATCH, $event);

        return $this->filterResponse(
            $request,
            $event->getResponse() ?: new Response('Not Found', 404),
            $type,
            Events::FILTER_RESPONSE_AFTER
        );
    }

    /**
     * fireRequestEvent
     *
     * @param Request $request
     * @param int $type
     *
     * @return EventInterface
     */
    protected function fireRequestEvent(Request $request, $type)
    {
        $this->events->dispatch(Events::REQUEST, $event = new GetResponse($this, $request, $type));

        return $event;
    }

    /**
     * fireDispatchEvent
     *
     * @param Request $request
     * @param int $type
     *
     * @return EventInterface
     */
    protected function fireDispatchEvent(Request $request, $type)
    {
        $this->events->dispatch(Events::DISPATCH, $event = new HandleRequestEvent($this, $request, $type));

        return $event;
    }

    /**
     * fireExceptionEvent
     *
     * @param Request $request
     * @param mixed $type
     * @param \Exception $exception
     *
     * @return ExceptionEventInterface
     */
    protected function fireExceptionEvent(Request $request, $type, \Exception $e)
    {
        $this->events->dispatch(Events::EXCEPTION, $event = new HandleException($this, $request, $type, $e));

        return $event;
    }

    /**
     * filterResponse
     *
     * @param Request  $request
     * @param Response $response
     * @param int      $type
     *
     * @return Resopnse
     */
    protected function filterResponse(Request $request, Response $response, $type, $eventName)
    {
        // Dispatch the `kernel.filter_response_*` event:
        $this->events->dispatch(
            $eventName,
            $event = new FilterResponse($this, $request, $type, $response)
        );

        $this->endRequest($request, $type);

        return $event->getResponse();
    }

    /**
     * handleRequestException
     *
     * @param Request    $request
     * @param \Exception $e
     * @param int        $type
     *
     * @return Response
     */
    protected function handleRequestException(Request $request, \Exception $e, $type)
    {
        $event = $this->fireExceptionEvent($request, $type, $e);

        if (!($response = $event->getResponse())) {
            $response = new Response($response);

            $filter = new ExceptionFilter;
            $filter->setException($e);
            $filter->filter($response);
        }

        try {
            $response = $this->filterResponse($request, $response, $type, Events::FILTER_RESPONSE_AFTER);
        } catch (\Exception $e) {
        }

        return $response;
    }

    /**
     * endRequest
     *
     * @param Request $request
     * @param mixed $type
     *
     * @access protected
     * @return void
     */
    protected function endRequest(Request $request, $type)
    {
        //$this->events->dispatch(
            //Events::END_REQUEST,
            //new HandleRequestEndEvent($this, $request, $type)
        //);

        $this->requestStack->pop();
    }
}
