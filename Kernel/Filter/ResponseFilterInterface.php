<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Filter package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Filter;

use \Symfony\Component\HttpFoundation\Response;

/**
 * @interface ResponseFilterInterface
 *
 * @package Selene\Adapter\Kernel\Filter
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface ResponseFilterInterface
{
    /**
     * filter
     *
     * @param Response $response
     * @param \Exception $exception
     *
     * @return void
     */
    public function filter(Response $response);
}
