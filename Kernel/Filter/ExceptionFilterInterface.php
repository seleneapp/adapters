<?php

/*
 * This File is part of the Selene\Adapter\Kernel\Filter package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel\Filter;

/**
 * @class ExceptionFilterInterface
 *
 * @package Selene\Adapter\Kernel\Filter
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface ExceptionFilterInterface extends ResponseFilterInterface
{
    public function setException(\Exception $e);
}
