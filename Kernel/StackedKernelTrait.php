<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel;

use \Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * @trait StackedKernelTrait
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
trait StackedKernelTrait
{
    private $kernel;

    /**
     * priority
     *
     * @var int
     */
    protected $priority;

    /**
     * setKernel
     *
     * @param HttpKernelInterface $kernel
     *
     * @return void
     */
    public function setKernel(HttpKernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * getKernel
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface;
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * get the priority on the kernel stack
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority ?: 1000;
    }
}
