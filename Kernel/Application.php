<?php

/**
 * This File is part of the Selene\Adapter\Kernel package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Kernel;

use \Selene\Module\Config\CacheInterface;
use \Selene\Module\Config\Loader\DelegatingLoader;
use \Selene\Module\Config\Loader\Resolver as LoaderResolver;
use \Selene\Module\Config\Resource\Locator;
use \Selene\Module\DI\ContainerCache;
use \Selene\Module\DI\Parameters;
use \Selene\Module\DI\Builder;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\Processor;
use \Selene\Module\DI\Processor\Configuration;
use \Selene\Module\DI\Dumper\ContainerGenerator;
use \Selene\Module\DI\ContainerAwareInterface;
use \Selene\Module\DI\Traits\ContainerAwareTrait;
use \Selene\Module\DI\Loader\XmlLoader;
use \Selene\Module\DI\Loader\PhpLoader;
use \Selene\Module\DI\Loader\CallableLoader;
use \Selene\Module\Events\DispatcherInterface;
use \Selene\Module\Package\PackageRepository;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\HttpKernelInterface;
use \Symfony\Component\HttpKernel\TerminableInterface;
use \Selene\Module\Common\Helper\ListHelper;
use \Composer\Autoload\ClassLoader;

/**
 * @class Application implements HttpKernelInterface, TerminableInterface, ContainerAwareInterface
 * @see HttpKernelInterface
 * @see TerminableInterface
 * @see ContainerAwareInterface
 *
 * @package Selene\Adapter\Kernel
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class Application implements ApplicationInterface, TerminableInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * debugger
     *
     * @var Selene\Module\Kernel\Debugger
     */
    public $debugger;

    /**
     * configLoader
     *
     * @var mixed
     */
    protected $configLoader;

    /**
     * booted
     *
     * @var boolean
     */
    protected $booted;

    /**
     * packages
     *
     * @var \Selene\Module\Package\PackageRepository
     */
    protected $packages;

    /**
     * includePhpResources
     *
     * @var array
     */
    protected $packageResources;

    /**
     * containerCachePath
     *
     * @var void
     */
    protected $containerCachePath;

    /**
     * packageProviders
     *
     * @var array
     */
    protected $packageProviders;

    /**
     * applicationRoot
     *
     * @var string
     */
    protected $kernelRoot;

    protected $autoloader;

    /**
     * version
     *
     * @var string
     */
    protected static $version = '1.0.0 pre alpha';

    /**
     * testEnv
     *
     * @var string
     */
    protected static $testEnv = 'testing';

    /**
     * prodEnv
     *
     * @var string
     */
    protected static $prodEnv = 'production';

    /**
     * beforeBuildPackages
     *
     * @var array
     */
    protected $beforeBuildPackages;

    /**
     * afterBuildPackages
     *
     * @var array
     */
    protected $afterBuildPackages;

    /**
     * Constructor.
     *
     * @param string $environment
     * @param boolean $debug
     */
    public function __construct($environment, $debug = true)
    {
        $this->env = $environment;

        if ((bool)$debug) {
            $this->debugger = new Debugger;
            $this->debugger->start();
        }

        $this->packageResources    = [];
        $this->packageProviders    = [];
        $this->beforeBuildPackages = [];
        $this->afterBuildPackages  = [];
    }

    /**
     * setAutoloader
     *
     * @param object $loader
     *
     * @return void
     */
    public function setAutoloader($loader)
    {
        $this->autoloader = $loader;
    }

    /**
     * getAutoloader
     *
     * @return void
     */
    public function getAutoloader()
    {
        return $this->autoloader;
    }

    /**
     * version
     *
     * @return mixed
     */
    public static function version()
    {
        return static::$version;
    }

    /**
     * Boots the application
     *
     * Initialize packages, initialize the service container, boot the kernel
     * stack, and prepare the controller resolver.
     *
     * @return boolean
     */
    public function boot()
    {
        if ($this->booted) {
            return false;
        }

        //var_dump('booting');
        //$start = microtime(true);
        $this->initializePackages();
        //var_dump(sprintf('done init packages %s', 1000 * (microtime(true) - $start)));
        //$start = microtime(true);
        $this->initializeContainer();
        //$start = microtime(true);
        //var_dump(sprintf('done init container %s', 1000 * (microtime(true) - $start)));
        //$stop = microtime(true);
        //$start = microtime(true);
        $this->injectServicesOnBoot();
        //var_dump(sprintf('done injectingservices %s', 1000 * (microtime(true) - $start)));

        //$start = microtime(true);
        $this->packages->boot($this);
        //var_dump(sprintf('done booting packages %s', 1000 * (microtime(true) - $start)));

        return $this->booted = true;
    }

    /**
     * Handle a http request.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $type
     * @param boolean $catch
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        try {
            $start = microtime(true);
            $this->boot();
            $stop = microtime(true);
            //var_dump(sprintf('booted: %s', 1000 * ($stop - $start)));

            return $this->getKernelStack()->handle($request, $type, $catch);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * run
     *
     * @param Request $request
     *
     * @return void
     */
    public function run(Request $request = null, $catch = true)
    {
        $request = $request ?: Request::createFromGlobals();

        $doCatch = null !== $catch ? (bool)$catch : self::$prodEnv === $this->getEnvironment();

        $response = $this->handle($request, self::MASTER_REQUEST, $doCatch);

        $response->send();

        $this->terminate($request, $response);
    }

    /**
     * Terminates the application
     *
     * @param Request $request
     * @param Response $response
     *
     * @return void
     */
    public function terminate(Request $request, Response $response)
    {
        $this->getKernelStack()->terminate($request, $response);

        if ($this->debugger) {
            $this->debugger->stop();
        }

        $this->packages->shutDown($this);
    }

    /**
     * Get the application kernel.
     *
     * @return \Symfony\Component\HttpFoundation\HttpKernelInterface
     */
    public function getKernel()
    {
        return $this->getContainer()->get($this->getKernelServiceId());
    }

    /**
     * Get the current application envirnonment.
     *
     * @return string
     */
    public function getEnvironment()
    {
        return $this->env;
    }

    /**
     * Set the Application directory
     *
     * @param mixed $path
     *
     * @return void
     */
    public function setKernelRoot($path)
    {
        $this->kernelRoot = $path;
    }

    /**
     * Get the application root directory.
     *
     * @return string
     */
    public function getKernelRoot()
    {
        if (null === $this->kernelRoot) {
            $this->kernelRoot = $this->guessApplicationPath();
        }

        return $this->kernelRoot;
    }

    /**
     * Set the path to the container cache.
     *
     * @param string $path
     *
     * @return void
     */
    public function setContainerCachePath($path)
    {
        $this->containerCachePath = $path;
    }

    /**
     * Get the path to the container cache.
     *
     * @return string
     */
    public function getContainerCachePath()
    {
        return $this->containerCachePath;
    }

    /**
     * Get the package repository.
     *
     * @return PackageRepository
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Add a file path from which a package provider schold be loaded.
     *
     * @param string $path the path to the php file that returns an array of
     * package provides.
     * @param string $extension the file extension.
     *
     * @return void
     */
    public function addPackageResource($path, $extension = '.php')
    {
        if (!file_exists($file = dirname($path).DIRECTORY_SEPARATOR.basename($path, $extension).$extension)) {
            return;
        }

        $this->packageResources[] = $file;
        $this->packageProviders[] = include $file;
    }

    /**
     * guessApplicationPath
     *
     * @return string
     */
    protected function guessApplicationPath()
    {
        $reflection = new \ReflectionObject($this);

        return dirname($reflection->getFileName());
    }

    /**
     * getKernelStack
     *
     * @return \Selene\Adapter\Kernel\Stack
     */
    abstract public function getKernelStack();

    /**
     * getApplicationServiceId
     *
     * @return string
     */
    abstract public function getApplicationServiceId();

    /**
     * getContainerServiceName
     *
     * @return string
     */
    abstract public function getContainerServiceId();

    /**
     * getRequestStack
     *
     * @return RequestStack
     */
    abstract public function getRequestStack();

    /**
     * isDebugging
     *
     * @return boolean
     */
    public function isDebugging()
    {
        return null !== $this->debugger;
    }

    /**
     * runsInConsole
     *
     * @return mixed
     */
    public function runsInConsole()
    {
        return 'cli' === php_sapi_name();
    }

    /**
     * runsInTest
     *
     * @return mixed
     */
    public function runsInTest()
    {
        return static::$testEnv === $this->getEnvironment();
    }

    /**
     * onPackagesPreBuild
     *
     * @param callable $callback
     *
     * @return void
     */
    public function onPackagesPreBuild(callable $callback)
    {
        $this->beforeBuildPackages[] = $callback;
    }

    /**
     * onPackagesPostBuild
     *
     * @param callable $callback
     *
     * @return void
     */
    public function onPackagesPostBuild(callable $callback)
    {
        $this->afterBuildPackages[] = $callback;
    }

    /**
     * setTestEnvironmentName
     *
     * @param mixed $name
     *
     * @return mixed
     */
    public static function setTestEnvironmentName($name)
    {
        static::$testEnv = strtolower($name);
    }

    /**
     * getContainerClass
     *
     * @return string
     */
    protected function getContainerClass()
    {
        return 'Selene\Module\DI\Container';
    }

    /**
     * Inject services to the container.
     *
     * @return void
     */
    abstract protected function injectServicesOnBoot();

    /**
     * initializeContainer
     *
     * @return void
     */
    protected function initializeContainer()
    {
        $cache = $this->getContainerCache();

        //throw new \Exception;
        try {

            if (!$cache->isValid()) {
                $this->doBuildContainer($cache);
            }

            $container = $cache->load();
            $this->setContainer($container);

        } catch (\Exception $e) {
            throw new \RuntimeException(
                sprintf(
                    'Loading container cache failed with error: "%s" in %s on line %s',
                    $e->getMessage(),
                    $e->getFile(),
                    $e->getLine()
                )
            );
        }
    }

    /**
     * getConfigCache
     *
     * @return ConfigCache
     */
    protected function getContainerCache()
    {
        return new ContainerCache(
            $this->getCachedContainerClassName(),
            $this->getCachedContainerFileName(),
            $this->isDebugging()
        );
    }

    /**
     * getCachedContainerClassName
     *
     * @return string
     */
    protected function getCachedContainerClassName()
    {
        return sprintf(
            '%s\%s',
            $this->getCachedContainerNameSpace(),
            $this->getCachedContainerBaseName()
        );
    }

    /**
     * getCachedContainerFileName
     *
     * @return string
     */
    protected function getCachedContainerFileName()
    {
        return sprintf(
            '%s%s%s.php',
            $this->getContainerCachePath(),
            DIRECTORY_SEPARATOR,
            $this->getCachedContainerBaseName()
        );
    }


    /**
     * getCachedContainerBaseName
     *
     * @return string
     */
    protected function getCachedContainerBaseName()
    {
        return 'Container'.ucfirst($this->getEnvironment());
    }

    /**
     * getCachedContainerNameSpace
     *
     * @return string
     */
    protected function getCachedContainerNameSpace()
    {
        list($rootNs,) = explode('\\', __NAMESPACE__);

        return $rootNs.'\ClassCache';
    }

    /**
     * buildContainer
     *
     * @param ConfigCache $cache
     *
     * @return BuilderInterface
     */
    protected function buildContainer(CacheInterface $cache, $containerClass, $containerFile)
    {
        $class = $this->getContainerClass();

        $this->containerPreBuild(
            $container = new $class(new Parameters($this->getDefaultParameters())),
            $containerClass,
            $containerFile
        );

        $this->setContainer($container);

        $container->inject($this->getApplicationServiceId(), $this);
        $container->inject($this->getContainerServiceId(), $container);

        $builder = $this->getContainerBuilder($container);
        $builder->addFileResource(__FILE__);
        $builder->addObjectResource($this);

        $this->buildPackages($builder);

        $builder->build();

        return $builder;
    }

    /**
     * buildPackages
     *
     * @param BuilderInterface $builder
     *
     * @return void
     */
    protected function buildPackages(BuilderInterface $builder)
    {
        $loader = $this->getConfigLoader($builder);

        foreach ($this->packageResources as $file) {
            $builder->addFileResource($file);
        }

        $loader->load('config.xml', true);
        $loader->load('config_'.strtolower($this->getEnvironment()).'.xml', true);

        $this->packageBuildCallback($this->beforeBuildPackages);
        $this->packages->build($builder);
        $this->packageBuildCallback($this->afterBuildPackages);
    }

    /**
     * doBuildContainer
     *
     * @param ConfigCache $cache
     *
     * @return void
     */
    protected function doBuildContainer(CacheInterface $cache)
    {
        $builder = $this->buildContainer(
            $cache,
            $class = $this->getCachedContainerClassName(),
            $file  = $this->getCachedContainerFileName()
        );

        $dumper = $this->getContainerDumper(
            $builder->getContainer(),
            $this->getCachedContainerNamespace(),
            $this->getCachedContainerBaseName(),
            $this->getContainerServiceId()
        );

        $cache->write($dumper->generate(), $builder->getResources(), true);
    }

    /**
     * packageBuildCallback
     *
     * @param array $callbacks
     *
     * @return void
     */
    protected function packageBuildCallback(array &$callbacks)
    {
        while (0 < count($callbacks)) {
            call_user_func(array_shift($callbacks), $this->packages);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultParameters()
    {
        $aid = $this->getApplicationServiceId();
        $kid = $this->getKernelServiceId();

        return array_merge(
            $this->getPackageInfo($aid),
            [
                $kid.'.root'      => $this->getKernelRoot(),
                $aid.'.root'      => $this->getKernelRoot(),
                $aid.'.env'       => $this->getEnvironment(),
                $aid.'.debugging' => null !== $this->debugger,
            ]
        );
    }

    /**
     * containerPreBuild
     *
     * @param ContainerInterface $container
     * @param string $containerClass
     * @param string $containerFile
     *
     * @return void
     */
    abstract protected function containerPreBuild(ContainerInterface $container, $containerClass, $containerFile);

    /**
     * getConfigLoader
     *
     * @param BuilderInterface $builder
     * @param string $rootPath
     *
     * @return Slene\Module\Config\Loader\LoaderInterface
     */
    protected function getConfigLoader(BuilderInterface $builder)
    {
        $rootPath = $this->getKernelRoot().DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'packages';

        $locator = new Locator($this->getPackageConfig(), $rootPath);

        $resolver = new LoaderResolver([
          new XmlLoader($builder, $locator),
          new PhpLoader($builder, $locator),
          new CallableLoader($builder)
        ]);

        return new DelegatingLoader($resolver);
    }

    /**
     * getContainerBuilder
     *
     * @param ContainerInterface $container
     *
     * @return BuilderInterface
     */
    protected function getContainerBuilder(ContainerInterface $container)
    {
        return new Builder($container, new Processor(new Configuration));
    }

    /**
     * getContainerDumper
     *
     * @param ContainerInterface $container
     * @param string $namespace
     * @param string $className
     * @param string $id
     *
     * @return ContainerGenerator
     */
    protected function getContainerDumper(ContainerInterface $container, $namespace, $className, $id)
    {
        return new ContainerGenerator($container, $namespace, $className, $id);
    }

    /**
     * loadContainerCache
     *
     * @return void
     */
    protected function loadContainerCache(CacheInterface $cache)
    {
        if ($container = $cache->load()) {
            $this->setContainer($container);

            return;
        }

        throw new \RuntimeException('could not load container cache.');
    }

    /**
     * initializePackages
     *
     * @return void
     */
    protected function initializePackages()
    {
        $class = $this->getPackageRepositoryClass();
        $this->packages = $this->packages ?:
            new $class($this->initPackages($this->getPackageResources()));
    }

    protected function getPackageRepositoryClass()
    {
        return  'Selene\Module\Package\PackageRepository';
    }

    /**
     * getPackageConfig
     *
     * @return array
     */
    protected function getPackageConfig()
    {
        $paths = [];

        foreach ($this->packages as $package) {
            $paths[] = $package->getAlias();
        }

        return $paths;
    }

    /**
     * initPackages
     *
     * @param array $packages
     *
     * @return array
     */
    protected function initPackages(array $packages)
    {
        $initialized = [];

        foreach ($packages as $packageClass) {
            if (!class_exists($packageClass)) {
                throw new \InvalidArgumentException(
                    sprintf('Package class "%s" does not exist.', $packageClass)
                );
            }

            $initialized[] = new $packageClass;

        }

        return $initialized;
    }

    /**
     * getPackages
     *
     * @return array
     */
    protected function getPackageResources()
    {
        $paths = ListHelper::arrayFlatten($this->packageProviders);

        return $paths;
    }

    /**
     * In addition to the default kernel parameter, this adds both the package
     * namespaces and package paths to the parameter list.
     *
     * @param
     * @return array
     *
     * ```
     * [
     *      packages      => [],
     *      package.paths => []
     * ]
     * ```
     */
    protected function getPackageInfo($prefix = '')
    {
        $info = [];
        $path = [];

        $prefix = 0 === strlen($prefix) ? '' : $prefix . '.';

        foreach ($this->getPackages() as $package) {
            $info[$package->getAlias()] = $package->getNamespace();
            $path[$package->getAlias()] = $package->getPath();
        }

        return [
            $prefix.'packages' => $info,
            $prefix.'package.paths' => $path
        ];
    }
}
