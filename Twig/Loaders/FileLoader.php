<?php

/**
 * This File is part of the Selene\Adapter\Twig\Loaders package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig\Loaders;

use \Twig_LoaderInterface as LoaderInterface;
use \Twig_ExistsLoaderInterface as ExistsLoaderInterface;
use \Selene\Module\View\Template\ResolverInterface;
use \Selene\Module\View\Template\LoaderInterface as TemplateLoaderInterface;

/**
 * @class FileLoader
 * @package Selene\Adapter\Twig\Loaders
 * @version $Id$
 */
class FileLoader implements LoaderInterface, TemplateLoaderInterface, ExistsLoaderInterface
{
    /**
     * paths
     *
     * @var array
     */
    protected $paths;

    /**
     * cache
     *
     * @var array
     */
    protected $cache;

    /**
     * parser
     *
     * @var PathParser
     */
    protected $parser;

    protected $locator;

    /**
     * Creates a new Fileloader instance.
     *
     * @param ResolverInterface $resolver
     */
    public function __construct(ResolverInterface $resolver, ViewLocatorInterface $locator = null)
    {
        $this->cache = [];

        $this->locator  = $locator;
        $this->resolver = $resolver;
    }

    /**
     * getSource
     *
     * @param mixed $source
     *
     * @return string
     */
    public function getSource($name)
    {
        return file_get_contents($this->findTemplate($name));
    }

    /**
     * load
     *
     * @param string $name
     *
     * @return string
     */
    public function load($name)
    {
        return $this->findTempalte($name);
    }

    /**
     * findTemplate
     *
     * @param mixed $name
     *
     * @return string
     */
    protected function findTemplate($name)
    {
        return $this->resolver->resolve($name);
    }

    /**
     * getCacheKey
     *
     * @param mixed $name
     *
     * @return string
     */
    public function getCacheKey($name)
    {
        $key = hash('sha256', $name);

        return $key;
    }

    /**
     * isFresh
     *
     * @param string $name
     * @param int $time
     *
     * @return boolean
     */
    public function isFresh($name, $time)
    {
        return filemtime($this->findTemplate($name)) < $time;
    }

    /**
     * isValid
     *
     * @param string $name
     * @param int $time
     *
     * @return boolean
     */
    public function isValid($name, $time)
    {
        return $this->isFresh($name, $time);
    }

    /**
     * exists
     *
     * @param string $name
     *
     * @return boolean
     */
    public function exists($name)
    {
        try {
            $this->findTemplate($name);
        } catch (\InvalidArgumentException $e) {
            return false;
        }

        return true;
    }
}
