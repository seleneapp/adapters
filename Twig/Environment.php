<?php

/**
 * This File is part of the Selene\Adapter\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig;

/**
 * @class Environment extends \Twig_Environment
 * @see \Twig_Environment
 *
 * @package Selene\Adapter\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Environment extends \Twig_Environment
{
    protected $view;

    /**
     * Constructor
     *
     * @param \Twig_LoaderInterface $loader
     * @param mixed $options
     *
     * @access public
     * @return mixed
     */
    public function __construct(\Twig_LoaderInterface $loader = null, $options = [])
    {
        parent::__construct(
            $loader,
            array_merge($options, ['base_template_class' => __NAMESPACE__ .'\\Template'])
        );
    }

    /**
     * setView
     *
     * @param mixed $view
     *
     * @return void
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * getView
     *
     * @return void
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * loadTemplate
     *
     * @param mixed $name
     * @param mixed $index
     *
     * @return void
     */
    public function loadTemplate($name, $index = null)
    {
        $template = parent::loadTemplate($name, $index);

        if ($template instanceof Template && null !== $this->view) {
            $template->setView($this->view);
        }

        return $template;
    }

    public function mergeShared(array $data)
    {
        return $data;
    }
}
