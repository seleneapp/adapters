<?php

/**
 * This File is part of the Selene\Adapter\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig;

use \Selene\Module\View\Template\EngineInterface;
use \Selene\Module\View\Template\ResolverInterface;

/**
 * @class TwigEngine implements EngineInterface
 * @see EngineInterface
 *
 * @package Selene\Adapter\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class TwigEngine implements EngineInterface
{
    /**
     * env
     *
     * @var Twig
     */
    private $env;

    /**
     * templateResolver
     *
     * @var mixed
     */
    private $templateResolver;

    /**
     * view
     *
     * @var mixed
     */
    private $view;

    /**
     * @param TwigEnvironment $twig
     *
     * @access public
     * @return mixed
     */
    public function __construct(Environment $twig, ResolverInterface $templateResolver)
    {
        $this->env = $twig;
        $this->templateResolver = $templateResolver;
    }

    public function getType()
    {
        return 'twig';
    }

    public function setView($view)
    {
        $this->env->setView($view);
    }

    /**
     * render
     *
     * @param mixed $view
     * @param mixed $context
     *
     * @access public
     * @return string
     */
    public function render($template, array $context = [])
    {
        return $this->load($template)->render($context);
    }

    /**
     * exists
     *
     * @param mixed $name
     *
     * @return boolean
     */
    public function exists($name)
    {
        if ($name instanceof \Twig_Template) {
            return true;
        }

        $loader = $this->env->getLoader();

        try {
            $loader->getSource($name);
        } catch (\Twig_Error_Loader $e) {
            return false;
        }

        return true;
    }

    /**
     * supports
     *
     * @param mixed $extension
     *
     * @access public
     * @return boolean
     */
    public function supports($name)
    {
        return $name instanceof Template || 'twig' === $this->templateResolver->resolve($name)->getEngine();
    }

    /**
     * load
     *
     * @param mixed $template
     *
     * @access protected
     * @return mixed
     */
    protected function load($template)
    {
        if ($template instanceof Template) {
            return $template;
        }

        return $this->env->loadTemplate((string)$template);
    }
}
