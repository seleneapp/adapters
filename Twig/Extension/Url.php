<?php

/*
 * This File is part of the Selene\Adapter\Twig\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig\Extension;

use \Selene\Module\Routing\UrlBuilder;

/**
 * @class Url
 * @package Selene\Adapter\Twig\Extension
 * @version $Id$
 */
class Url extends \Twig_Extension
{
    /**
     * parameters
     *
     * @var ParameterInterface
     */
    private $url;

    /**
     * Constructor.
     *
     * @param ParameterInterface $parameters
     */
    public function __construct(UrlBuilder $url)
    {
        $this->url = $url;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'selene_routing_url';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('url', function ($name, array $params = [], $host = null, $relative = true) {
                return $this->url->getPath($name, $params, $host, $this->getPathType($relative));
            }),
            new \Twig_SimpleFunction('current_url', function ($relative = true) {
                return $this->url->currentUrl($this->getPathType($relative));
            }),
            new \Twig_SimpleFunction('current_path', function ($relative = true) {
                return $this->url->currentPath($this->getPathType($relative));
            })
        ];
    }

    private function getPathType($relative)
    {
        return $relative ? UrlBuilder::RELATIVE_PATH : UrlBuilder::ABSOLUTE_PATH;
    }
}
