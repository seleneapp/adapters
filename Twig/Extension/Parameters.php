<?php

/**
 * This File is part of the Selene\Adapter\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig\Extension;

use \Selene\Module\DI\ParameterInterface;

/**
 * @class Parameters extends \Twig_Extension
 * @see \Twig_Extension
 *
 * @package Selene\Adapter\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Parameters extends \Twig_Extension
{
    /**
     * parameters
     *
     * @var ParameterInterface
     */
    private $parameters;

    /**
     * Constructor.
     *
     * @param ParameterInterface $parameters
     */
    public function __construct(ParameterInterface $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Selene_DI_Parameters';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('param_get', function ($key) {
                return $this->parameters->get($key);
            }),
            new \Twig_SimpleFunction('param_resolve', function ($value) {
                return is_string($value) ? $this->parameters->resolve($vaue) : $this->parameters->resolveParam($vaue);
            })
        ];
    }
}
