<?php

/**
 * This File is part of the Selene\Adapter\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig\Process;

use \Selene\Module\DI\CallerReference;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * @class RegisterExtensions
 * @package Selene\Adapter\Twig\Process
 * @version $Id$
 */
abstract class PrepareExtensions implements ProcessInterface
{
    const EXT_ALL = true;

    private $tag;
    private $container;
    private $extensions;
    private $containerId;

    public function __construct($tag = 'twig.extension', $extensions = self::EXT_ALL)
    {
        $this->tag = $tag;
        $this->extensions = $extensions;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        $this->container = $container;

        $this->register(
            $list = $this->getExtensionList(),
            self::EXT_ALL === $this->extensions ? array_keys($list) : (array)$this->extensions
        );
    }

    protected function setContainerId($id)
    {
        $this->containerId = $id;
    }

    protected function getContainerId()
    {
        return $this->containerId;
    }

    protected function getExtensions()
    {
        return $this->extensions;
    }

    protected function setExtensions(array $extensions)
    {
        $this->extensions = $extensions;
    }

    /**
     * register
     *
     * @param ServiceDefinition $definition
     * @param array $extensions
     * @param array $enabled
     *
     * @return void
     */
    private function register(array $extensions, array $enabled)
    {
        if (empty($enabled)) {
            return;
        }

        $internals = array_keys($this->getAdapterExtensionList());

        foreach ($extensions as $key => $class) {
            $this->registerExtension($key, $class, $internals);
        }
    }

    /**
     * registerExtension
     *
     * @param mixed $def
     * @param mixed $ext
     * @param mixed $extClass
     *
     * @access private
     * @return void
     */
    private function registerExtension($ext, $extClass, array $internals = [])
    {
        $def = $this->container->define($id = 'twig.extension_'.$ext, $extClass);
        $def->setInternal(true)->setMetaData($this->tag);

        if (in_array($ext, $internals)) {
            $def->setArguments($this->getInternalArguments($ext));
            //[new CallerReference($this->getContainerId(), 'getParameters')]);
        }
    }

    protected function getInternalArguments($name)
    {
        return [];
    }

    /**
     * getExtensionList
     *
     * @return array
     */
    private function getExtensionList()
    {
        return array_merge($this->getAdapterExtensionList(), [
            'array' => 'Twig_Extensions_Extension_Array',
            'date'  => 'Twig_Extensions_Extension_Date',
            'inlt'  => 'Twig_Extensions_Extension_Intl',
            'text'  => 'Twig_Extensions_Extension_Text'
        ]);
    }

    /**
     * getAdapterExtensionList
     *
     * @return array
     */
    private function getAdapterExtensionList()
    {
        return [
            'selene_di_parameters' => 'Selene\Adapter\Twig\Extension\Parameters',
            'selene_routing_url' => 'Selene\Adapter\Twig\Extension\Url'
        ];
    }
}
