<?php

/**
 * This File is part of the Selene\Package\Twig\Process package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig\Process;

use \Selene\Module\DI\Reference;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Definition\FlagInterface;
use \Selene\Module\DI\Processor\ProcessInterface;

/**
 * The class searches for all services that are marked as twig extensions and
 * registeres them to the twig wnvironment.
 *
 * @abstract class RegisterExtensions implements ProcessInterface
 * @see ProcessInterface
 * @abstract
 *
 * @package Selene\Adapter\Twig
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class RegisterExtensions implements ProcessInterface
{
    /**
     * envId
     *
     * @var string
     */
    private $envId;

    /**
     * metaName
     *
     * @var string
     */
    private $metaName;

    /**
     * {@inheritdoc}
     */
    public function process(ContainerInterface $container)
    {
        if (!$container->hasDefinition($envId = $this->getTwigEnvId())) {
            return;
        }

        $args = [];

        foreach ($container->findDefinitionsWithMetaData($this->getTwigExtensionMetaName()) as $id => $definition) {
            $args[] = new Reference($id);
        }

        $container->getDefinition($envId)->addSetter('setExtensions', [$args]);
    }

    /**
     * setTwigEnvId
     *
     * @param string $id
     *
     * @return void
     */
    protected function setTwigEnvId($id)
    {
        $this->envId = $id;
    }

    /**
     * getTwigEnvId
     *
     * @return string
     */
    protected function getTwigEnvId()
    {
        return $this->envId;
    }

    /**
     * setTwigExtensionMetaName
     *
     * @param name $name
     *
     * @return void
     */
    protected function setTwigExtensionMetaName($name)
    {
        $this->metaName = $name;
    }

    /**
     * getTwigExtensionMetaName
     *
     * @return string
     */
    protected function getTwigExtensionMetaName()
    {
        return $this->metaName ?: 'twig.extension';
    }
}
