<?php

/**
 * This File is part of the Selene\Adapter\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Twig;

use \Selene\Module\View\Renderer;
use \Selene\Module\View\DispatcherInterface;

/**
 * @class Template
 * @package Selene\Adapter\Twig
 * @version $Id$
 */
abstract class Template extends \Twig_Template
{
    protected $view;

    /**
     * display
     *
     * @param array $context
     * @param array $blocks
     *
     * @return void
     */
    public function display(array $context, array $blocks = array())
    {
        parent::display($c = $this->notifyMembers($context), $blocks);
    }

    /**
     * setView
     *
     * @param DispatcherInterface $view
     *
     * @return void
     */
    public function setView(DispatcherInterface $view)
    {
        $this->view = $view;
    }

    /**
     * notifyMembers
     *
     * @param array $context
     *
     * @return array
     */
    private function notifyMembers(array $context)
    {
        if (null === $this->view) {
            return $this->env->mergeShared($context);
        }

        $renderer = $this->newRenderer($this->view, $context);

        $this->view->notifyComposers($renderer);

        return $renderer->getContext();
    }

    /**
     * newRenderer
     *
     * @param DisptcherInterface $view
     * @param array $context
     *
     * @return RendererInterface
     */
    protected function newRenderer(DispatcherInterface $view, array $context = [])
    {
        return new Renderer($view, $view->findEngineByName('twig'), $this->getTemplateName(), $context);
    }

}
