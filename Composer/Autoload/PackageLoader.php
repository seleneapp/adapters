<?php

/*
 * This File is part of the Selene\Adapter\Composer\Autoload package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Composer\Autoload;

use \FileystemIterator;
use \Selene\Module\Filesystem\PatternIterator;

/**
 * @class PackageLoader
 *
 * @package Selene\Adapter\Composer\Autoload
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class PackageLoader
{
    private $dir;

    public function __construct($dir)
    {
        $this->dir = $dir;
    }

    /**
     * load
     *
     * @return void
     */
    public function load()
    {
        foreach ($this->getIterator() as $path) {
            include $path;
        }
    }

    /**
     * getIterator
     *
     * @return PatternIterator
     */
    private function getIterator()
    {
        return new PatternIterator(
            $this->dir,
            '~^autoload.php$~',
            4,
            PatternIterator::MATCH,
            FileystemIterator::SKIP_DOTS
        );
    }
}
