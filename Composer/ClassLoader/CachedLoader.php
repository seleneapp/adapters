<?php

/*
 * This File is part of the Selene\Adapter\Composer\ClassLoader package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Composer\ClassLoader;

use \Composer\Autoload\ClassLoader;
use \Selene\Module\Cache\Driver\ApcDriver;
use \Selene\Module\Cache\Driver\DriverInterface;

/**
 * @class CachedLoader
 *
 * @package Selene\Adapter\Composer\ClassLoader
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class CachedLoader extends ClassLoader
{
    /**
     * cache
     *
     * @var DriverInterface
     */
    private $cache;

    public function __construct(DriverInterface $cache = null)
    {
        $this->cache = $cache ?: new ApcDriver;
    }

    /**
     * loadClass
     *
     * @param string $class
     *
     * @return boolean|null
     */
    public function loadClass($class)
    {
        if ($file = $this->findFile($class)) {
            require $file;

            return true;
        }
    }

    /**
     * findFile
     *
     * @param string $class
     *
     * @return string
     */
    public function findFile($class)
    {
        if (!$file = $this->cache->getFromCache($key = $this->prefix.$class)) {
            $this->cache->writeToCache($key, $file = parent::findFile($class));
        }

        return $file;
    }
}
