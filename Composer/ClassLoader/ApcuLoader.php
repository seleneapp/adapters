<?php

/*
 * This File is part of the Selene\Adapter\Composer\ClassLoader package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Composer\ClassLoader;

use \Selene\Module\Cache\Driver\ApcuDriver;

/**
 * @class ApcLoader
 *
 * @package Selene\Adapter\Composer\ClassLoader
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class ApcuLoader extends ApcLoader
{
    /**
     * Constructor.
     *
     * @param ClassLoader $loader
     * @param string $prefix
     * @param ApcDriver $cache
     */
    public function __construct(ClassLoader $loader, $prefix = 'selene_c', ApcuDriver $cache = null)
    {
        $this->loader = $loader;
        $this->prefix = $prefix;

        $this->cache = $cache ?: new ApcuDriver;
    }

    /**
     * register
     *
     * @param mixed $prepend
     *
     * @return void
     */
    public function register($prepend = false)
    {
        spl_autoload_register([$this, 'loadClass'], true, $prepend);
    }

    /**
     * unregister
     *
     * @return void
     */
    public function unregister()
    {
        spl_autoload_unregister([$this, 'loadClass']);
    }

    /**
     * loadClass
     *
     * @param string $class
     *
     * @return boolean|null
     */
    public function loadClass($class)
    {
        if ($file = $this->findFile($class)) {
            require $file;

            return true;
        }
    }

    /**
     * findFile
     *
     * @param string $class
     *
     * @return string
     */
    public function findFile($class)
    {
        if (!$file = $this->cache->getFromCache($key = $this->prefix.$class)) {
            $this->cache->writeToCache($key, $file = $this->loader->findFile($class));
        }

        return $file;
    }

    public function __call($method, $arguments)
    {
        return call_user_func_array([$this->loader, $method], $arguments);
    }
}
