<?php

/**
 * This File is part of the Selene\Adapter\Console\Tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Console\Tests;

use \Mockery as m;
use \Selene\Module\TestSuite\TestCase;
use \Selene\Adapter\Console\Application;

/**
 * @class ApplicationTest
 * @package Selene\Adapter\Console\Tests
 * @version $Id$
 */
class ApplicationTest extends TestCase
{
    protected $app;

    /** @test */
    public function itShouldNotBeBooted()
    {
        $app = $this->newApp();
        $this->assertFalse($app->isBooted());
    }

    /** @test */
    public function appShouldBeGettable()
    {
        $app = $this->newApp();

        $this->assertSame($this->app, $app->getApplication());
    }

    /** @test */
    public function itShouldBeBootable()
    {
        $app = $this->newApp();

        $app->boot();
        $this->assertTrue($app->isBooted());
    }

    /** @test */
    public function itShouldAlwaysReturnLogger()
    {
        $app = $this->newApp();
        $this->assertInstanceof('Psr\Log\LoggerInterface', $app->getLogger());
    }

    /** @test */
    public function itShouldAlwaysReturnEventDispatcher()
    {
        $app = $this->newApp();
        $this->assertInstanceof('Selene\Module\Events\DispatcherInterface', $app->getEvents());
    }

    /** @test */
    public function eventsSouldBeSettable()
    {
        $app = $this->newApp();
        $app->setEvents($events = m::mock('Selene\Module\Events\DispatcherInterface'));
        $this->assertSame($events, $app->getEvents());
    }


    /** @test */
    public function loggerSouldBeSettable()
    {
        $app = $this->newApp();
        $app->setLogger($logger = m::mock('Psr\Log\LoggerInterface'));
        $this->assertSame($logger, $app->getLogger());
    }

    /** @test */
    public function itShouldLogOnBootException()
    {
        $called = false;
        $packages = [
            $p = m::mock('Selene\Module\Package\Package')
        ];

        $p->shouldReceive('registerCommands')->andReturnUsing(function () {
            throw new \Exception('error');
        });

        $app = $this->newApp($this->mockApp('1.0.0', $packages));
        $app->setLogger($logger = m::mock('Psr\Log\LoggerInterface'));

        $logger->shouldReceive('error')->with('error')->once()->andReturnUsing(function () use (&$called) {
            $called = true;
        });

        $app->boot();
        $this->assertTrue($called);
    }

    /** @test */
    public function itSetOutputOnLogger()
    {
        $set = false;
        $app = $this->newApp();
        $logger = m::mock('Psr\Log\LoggerInterface, Selene\Adapter\Console\OutputAware');
        $output = m::mock('Symfony\Component\Console\Output\OutputInterface');

        $logger->shouldReceive('setOutput')->with($output)->andReturnUsing(function ($out) use (&$set, &$output) {
            $set = true;
            $this->assertSame($output, $out);
        });

        $app->setLogger($logger);

        try {
            $app->run(null, $output);
        } catch (\Exception $e) {
        }

        $this->assertTrue($set, 'output should be set on logger.');
    }

    /** @test */
    public function itGetOutputFromLogger()
    {
        $called = false;
        $app = $this->newApp();
        $logger = m::mock('Psr\Log\LoggerInterface, Selene\Adapter\Console\OutputAware');
        $output = m::mock('Symfony\Component\Console\Output\OutputInterface');

        $logger->shouldReceive('getOutput')->andReturnUsing(function () use (&$called, &$output) {
            $called = true;

            return $output;
        });

        $app->setLogger($logger);

        try {
            $app->run();
        } catch (\Exception $e) {
        }

        $this->assertTrue($called, 'output should be set from logger.');
    }

    /** @test */
    public function itShouldRegisterConsoleOnPackages()
    {
        $called = false;
        $packages = [
            $p = m::mock('Selene\Module\Package\Package')
        ];

        $app = $this->newApp($this->mockApp('1.0.0', $packages));

        $p->shouldReceive('registerCommands')->with($app)->andReturnUsing(function () use (&$called) {
            $called = true;
        });

        $app->boot();
        $this->assertTrue($called);
    }

    protected function newApp($app = null, $name = 'testing')
    {
        return new Application($app ?: $this->mockApp(), $name);
    }

    /**
     * mockApp
     *
     * @return object
     */
    protected function mockApp($v = '1.0.0', $packages = [])
    {
        $app = m::mock('Selene\Module\Kernel\Application');
        $app->shouldReceive('version')->andReturn($v);
        $app->shouldReceive('boot');
        $app->shouldReceive('getPackages')->andReturn($packages);

        return $this->app = $app;
    }
}
