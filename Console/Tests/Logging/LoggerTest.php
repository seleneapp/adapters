<?php

/**
 * This File is part of the Selene\Adapter\Console\Tests\Logging package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Console\Tests\Logging;

use \Mockery as m;
use \Selene\Module\TestSuite\TestCase;
use \Selene\Adapter\Console\Logging\Logger;

/**
 * @class LoggerTest
 * @package Selene\Adapter\Console\Tests\Logging
 * @version $Id$
 */
class LoggerTest extends TestCase
{
    /** @test */
    public function itShouldBeInstantiable()
    {
        $this->assertInstanceof('Psr\Log\LoggerInterface', new Logger($this->mockOutput()));
    }

    /** @test */
    public function itIsExpectedThat()
    {
        $out = $this->mockOutput();
        $this->assertSame($out, (new Logger($out))->getOutput());
    }

    /** @test */
    public function outputIsAccessable()
    {
        $called = false;

        $out = $this->mockOutput();
        $out->shouldReceive('getVerbosity')->andReturnUsing(function () use (&$called) {
            $called = true;
        });

        $logger = new Logger($out);

        try {
            $logger->info('test');
        } catch (\Exception $e) {
        }

        $this->assertTrue($called);
    }

    /** @test */
    public function outPutSouldBeSettable()
    {
        $out = $this->mockOutput();
        $logger = new Logger($out);

        $out = $this->mockOutput();

        $logger->setOutput($out);

        $this->assertSame($out, $logger->getOutput());
    }

    /**
     * mockOutput
     *
     * @return Symfony\Component\Console\Output\OutputInterface
     */
    protected function mockOutput()
    {
        return m::mock('Symfony\Component\Console\Output\ConsoleOutputInterface');
    }
}
