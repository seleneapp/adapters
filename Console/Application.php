<?php

/*
 * This File is part of the Selene\Module\Console package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Console;

use \Psr\Log\LogLevel;
use \Psr\Log\LoggerInterface;
use \Selene\Adapter\Console\Logging\Logger;
use \Selene\Module\Events\Dispatcher;
use \Selene\Module\Events\DispatcherInterface;
use \Selene\Adapter\Kernel\ApplicationInterface;
use \Symfony\Component\HttpKernel\HttpKernelInterface;
use \Symfony\Component\Console\Input\ArrayInput;
use \Symfony\Component\Console\Output\NullOutput;
use \Symfony\Component\Console\Input\InputOption;
use \Symfony\Component\Console\Output\ConsoleOutput;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Command\Command as SymfonyCommand;
use \Symfony\Component\Console\Application as ConsoleApplication;
use \Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @class Application extends ConsoleApplication
 * @see ConsoleApplication
 *
 * @package Selene\Module\Console
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Application extends ConsoleApplication
{
    /**
     * events
     *
     * @var DispatcherInterface
     */
    private $dispatcher;

    /**
     * logger
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * booted
     *
     * @var boolean
     */
    private $booted;

    /**
     * stat
     *
     * @var \SplStack
     */
    private $stat;

    /**
     * Constructor.
     *
     * @param HttpKernelInterface $app
     * @param string $name
     * @param string $version
     */
    public function __construct(HttpKernelInterface $app, $name)
    {
        parent::__construct($name, $app::version());

        $this->app = $app;
        $this->booted = false;
        $this->stat = new \SplStack;
    }

    /**
     * isBooted
     *
     * @return boolean
     */
    public function isBooted()
    {
        return $this->booted;
    }

    /**
     * setEvents
     *
     * @param DispatcherInterface $events
     *
     * @return void
     */
    public function setEvents(DispatcherInterface $events)
    {
        $this->dispatcher = $events;
    }

    /**
     * getEvents
     *
     * @return void
     */
    public function getEvents()
    {
        return $this->dispatcher ?: $this->dispatcher = new Dispatcher;
    }

    /**
     * Disable symfony event dispatcher.
     *
     * @param EventDispatcherInterface $dispatcher
     *
     * @return void
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
    }

    /**
     * setLogger
     *
     * @param LoggerInterface $logger
     *
     * @return void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * getVerbosityLevels
     *
     * @return array
     */
    public function getVerbosityLevels()
    {
        return [
            LogLevel::NOTICE => OutputInterface::VERBOSITY_VERBOSE
        ];
    }

    /**
     * getFormatLevels
     *
     * @return void
     */
    public function getFormatLevels()
    {
        return [
        ];
    }

    /**
     * Get the registered logger.
     *
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger ?: $this->newLogger();
    }

    /**
     * Gets a new ConsoleLogger.
     *
     * @return LoggerInterface
     */
    private function newLogger()
    {
        return $this->logger = new Logger(
            new ConsoleOutput,
            $this->getVerbosityLevels(),
            $this->getFormatLevels()
        );
    }

    /**
     * getApplication
     *
     * @return ApplicationInterface
     */
    public function getApplication()
    {
        return $this->app;
    }

    /**
     * initialize
     *
     * @return void
     */
    public function boot()
    {
        if ($this->isBooted()) {
            return false;
        }

        try {
            $this->bootApplication($this->app);
            $this->booted = true;
        } catch (\Exception $e) {
            $this->getLogger()->error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function run(InputInterface $input = null, OutputInterface $output = null)
    {
        $logger = $this->getLogger();

        if ($logger instanceof OutputAware) {
            if (null === $output) {
                $output = $logger->getOutput();
            } else {
                $logger->setOutput($output);
            }
        }

        $this->boot();

        $this->stat->push($status = parent::run($input, $output));

        return $status;
    }

    /**
     * Exits application with satus code
     *
     * @return void
     */
    public function shutDown()
    {
        exit($this->stat->pop());
    }

    /**
     * bootApplication
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    protected function bootApplication(ApplicationInterface $app)
    {
        $app->boot();

        foreach ($app->getPackages() as $package) {
            $package->registerCommands($this);
        }
    }

    /**
     * add
     *
     * @param SymfonyCommand $cmd
     *
     * @return void
     */
    public function add(SymfonyCommand $cmd)
    {
        return parent::add($cmd);
    }
}
