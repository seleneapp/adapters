<?php

/*
 * This File is part of the Selene\Adapter\Console\Logging package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Console\Logging;

use \Selene\Adapter\Console\OutputAware;
use \Symfony\Component\Console\Logger\ConsoleLogger;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\Console\Output\ConsoleOutputInterface;

/**
 * @class Logger
 * @package Selene\Adapter\Console\Logging
 * @version $Id$
 */
class Logger extends ConsoleLogger implements OutputAware
{
    /**
     * output
     *
     * @var OutPutInterface
     */
    protected $output;

    /**
     * Constructor.
     *
     * @param Outputinterface $output
     * @param array $verbosityLevelMap
     * @param array $formatLevelMap
     */
    public function __construct(Outputinterface $output, array $verbosityLevelMap = [], array $formatLevelMap = [])
    {
        parent::__construct($output, $verbosityLevelMap, $formatLevelMap);
        $this->output = $output;
    }

    /**
     * setOutput
     *
     * @param OutputInterface $output
     *
     * @return void
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * getOutput
     *
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }
}
