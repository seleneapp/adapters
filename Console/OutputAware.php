<?php

/*
 * This File is part of the Selene\Adapter\Console package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Console;

use \Symfony\Component\Console\Output\OutputInterface;

/**
 * @interface OutputAware
 * @package Selene\Adapter\Console
 * @version $Id$
 */
interface OutputAware
{
    public function setOutput(OutputInterface $output);

    public function getOutput();
}
