<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

/**
 * @class Manager
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 */
class Manager
{
    private $providers;

    /**
     *
     * @access public
     */
    public function __construct()
    {
        $this->providers = [];
    }

    public function add(ProviderInterface $provider)
    {
        return null;
    }

    public function getCache($type)
    {
        foreach ($this->providers as $provider) {
            if ($provider->supports($type)) {
                return $provider;
            }
        }

        throw new \RuntimeException(sprintf('No suitable provider found fot type %s', $type));
    }
}
