<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

use \Redis;
use \Doctrine\Common\Cache\RedisCache;

/**
 * @class RedisProvider implements ProviderInterface
 * @see ProviderInterface
 *
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class RedisProvider implements ProviderInterface
{
    public function __construct(ConnectionInterface $redisConnection)
    {
        $this->connection = $redisConnection;
    }

    /**
     * make
     *
     * @access public
     * @return mixed
     */
    public function create()
    {
        $this->connection->connect();

        return new RedisCache($this->connection->getDriver());
    }

    /**
     * supports
     *
     * @param mixed $provider
     *
     * @access public
     * @return boolean
     */
    public function supports($provider)
    {
        return 'redis' === $provider && extension_loaded('redis');
    }
}
