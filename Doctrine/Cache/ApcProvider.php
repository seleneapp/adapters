<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

use \Doctrine\Common\Cache\ApcCache;

/**
 * @class ApcProvider
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 */
class ApcProvider implements ProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function create()
    {
        return new ApcCache;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($provider)
    {
        return 'apc' === $provider && (extension_loaded('apc') || extension_loaded('apcu'));
    }
}
