<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

/**
 * @class ProviderInterface
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 */
interface ProviderInterface
{
    public function create();

    public function supports($provider);
}
