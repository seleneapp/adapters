<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

/**
 * @class NullProvider implements ProviderInterface
 * @see ProviderInterface
 *
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class NullProvider implements ProviderInterface
{
    public function create()
    {
        return null;
    }

    public function supports($type)
    {
        return 'null' === $type || null === $type;
    }
}
