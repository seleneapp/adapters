<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

use \Doctrine\Common\Cache\XcacheCache;

/**
 * @class XcacheProvider  XcacheProvider
 *
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class XcacheProvider implements ProviderInterface
{
    /**
     * make
     *
     *
     * @access public
     * @return mixed
     */
    public function create()
    {
        return new XcacheCache;
    }

    /**
     * supports
     *
     * @param mixed $provider
     *
     * @access public
     * @return boolean
     */
    public function supports($provider)
    {
        return 'xcache' === $provider && extension_loaded('xcache');
    }
}
