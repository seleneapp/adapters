<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Cache package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Cache;

use \Doctrine\Common\Cache\MemcacheCache;
use \Selene\Module\Cache\Driver\MemcacheConnection;

/**
 * @class MemcacheProvider
 * @package Selene\Adapter\Doctrine\Cache
 * @version $Id$
 */
class MemcacheProvider implements ProviderInterface
{
    private $connection;

    /**
     * @param MemcacheConnection $connection
     *
     * @access public
     * @return mixed
     */
    public function __construct(MemcacheConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * make
     *
     *
     * @access public
     * @return mixed
     */
    public function create()
    {
        $this->connection->connect();

        $cache = new MemcacheCache;
        $cache->setMemcache($this->connection->getDriver());

        return $cache;
    }

    /**
     * supports
     *
     * @param mixed $provider
     *
     * @access public
     * @return boolean
     */
    public function supports($provider)
    {
        return 'memcache' === $provider && extension_loaded('memcache');
    }
}
