<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Tests\Orm package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Tests\Orm;

use \Mockery as m;
use \Selene\Module\TestSuite\TestCase;
use \Selene\Adapter\Doctrine\Orm\EntityManagerProvider;


/**
 * @class EntityManagerProviderTest
 * @package Selene\Adapter\Doctrine\Tests\Orm
 * @version $Id$
 */
class EntityManagerProviderTest extends TestCase
{
    /** @test */
    public function itShouldBeInstantiable()
    {
        $this->assertInstanceof(
            '\Selene\Module\DI\ContainerAwareInterface',
            (new \ReflectionClass('Selene\Adapter\Doctrine\Orm\EntityManagerProvider'))
                ->newInstanceWithoutConstructor()
        );
    }

    /** @test */
    public function itShouldGetConnections()
    {
        $provider = $this->getManagerProvider();

        $this->container->shouldReceive('get')->with('connection.default')->andReturn(
            $con = $this->mockConnection()
        );

        $this->assertSame($con, $provider->getConnection());

        $provider = $this->getManagerProvider();

        $this->container->shouldReceive('get')->with('connection.foo')->andReturn(
            $con = $this->mockConnection()
        );

        $this->assertSame($con, $provider->getConnection('foo'));
    }

    /** @test */
    public function itShoudResolveNamespaceAliases()
    {
        $provider = $this->getManagerProvider();

        $this->container->shouldReceive('get')->andReturnUsing(function () {
            $m = $this->mockManager();
            $m->shouldReceive('getConfiguration')->andReturn($conf = m::mock('Configuration'));
            $conf->shouldReceive('getEntityNamespaces')->andReturn(
                []
            );

            $conf->shouldReceive('getEntityNamespace')->andReturn($m);
            return $m;
        });

        try {
            $provider->getManagerForClass('foo:'.__CLASS__);
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->assertTrue(true);
        }
    }

    protected function mockMetaDataFactory()
    {
        return m::mock('\Doctrine\DBAL\MetaDataFactory');
    }

    /**
     * mockConnection
     *
     * @return Mixed
     */
    protected function mockConnection()
    {
        return m::mock('\Doctrine\DBAL\Connection');
    }

    /**
     * mockConnection
     *
     * @return Mixed
     */
    protected function mockManager()
    {
        return m::mock('\Doctrine\ORM\EntityManager');
    }

    protected function getManagerProvider(array $connections = [], array $managers = [])
    {
        $this->container = $container = m::mock('Selene\Module\DI\ContainerInterface');
        $connections = array_merge(
            $connections,
            [
            'default' => 'connection.default',
            'foo'     => 'connection.foo',

            ]
        );

        $managers = array_merge(
            $managers,
            [
                'default' => 'manager.default',
                'foo' => 'manager.foo'
            ]
        );
        $default = 'default';

        return new EntityManagerProvider($container, $connections, $managers, $default, $default);
    }
}
