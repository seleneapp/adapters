<?php

/*
 * This File is part of the Selene\Adapter\Doctrine package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine;

use \Selene\Module\Events\Dispatcher;
use \Selene\Module\Events\DispatcherInterface;

/**
 * @class EventManager
 * @package Selene\Adapter\Doctrine
 * @version $Id$
 */
class EventManager extends DoctrineEventManager
{
    public function __construct(DispatcherInterface $events = null)
    {
        $this->events = $events ?: new Dispatcher;
    }

    public function dispatchEvent($eventName, EventArgs $eventArgs = null)
    {
        return $this->events->dispatch($eventName, $eventArgs);
    }
}
