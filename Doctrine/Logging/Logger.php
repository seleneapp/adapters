<?php

/*
 * This File is part of the Selene\Adapter\Doctrine package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Logging;

use \Psr\Log\LoggerInterface;
use \Doctrine\DBAL\Logging\SQLLogger;

/**
 * @class Logger extends SqlLogger
 * @see SqlLogger
 *
 * @package Selene\Adapter\Doctrine\Logging
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Logger implements SqlLogger
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->logger->debug('start query');
    }

    public function stopQuery()
    {
        $this->logger->debug('stop query');
    }
}
