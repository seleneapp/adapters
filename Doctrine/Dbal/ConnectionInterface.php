<?php

/*
 * This File is part of the Selene\Adapter\Doctrine\Dbal package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Dbal;

use \Selene\Module\Events\Dispatcher;
use \Selene\Module\Events\DispatcherInterface;

/**
 * @interface ConnectionInterface
 * @package Selene\Adapter\Doctrine\Dbal
 * @version $Id$
 */
interface ConnectionInterface
{
    /**
     * connect
     *
     * @return boolean
     */
    public function connect();

    /**
     * getConnection
     *
     * @return mixed
     */
    public function getConnection();

    /**
     * Create a new connection.
     *
     * @param array $params
     * @param DispatcherInteface $events
     *
     * @return ConnectionInterface
     */
    public static function create(array $params, DispatcherInterface $events = null);
}
