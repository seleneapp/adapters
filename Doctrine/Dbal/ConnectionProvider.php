<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Orm package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Orm;

use \Doctrine\DBAL\Connection;
use \Doctrine\Common\Persistence\ConnectionRegistry;
use \Selene\Module\DI\ContainerAwareInterface;
use \Selene\Module\DI\Traits\ContainerAwareTrait;

/**
 * @class EntityManagerProvider
 * @package Selene\Adapter\Doctrine\Orm
 * @version $Id$
 */
class ConnectionProvider implements ConnectionRegistry, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * managers
     *
     * @var array
     */
    private $connections;

    /**
     * managers
     *
     * @var array
     */
    private $builders;

    /**
     * default
     *
     * @var mixed
     */
    private $default;

    /**
     * Constructor.
     *
     * @param array $connections
     * @param string $default
     */
    public function __construct(array $connections = [], $default = null)
    {
        $this->builders = [];
        $this->default = $default;
        $this->setConnections($connections);
    }

    /**
     * setDefaultConnection
     *
     * @param string $default
     *
     * @return void
     */
    public function setDefaultConnectionName($default)
    {
        $this->default = $default;
    }

    /**
     * getDefaultConnectionName
     *
     * @return string
     */
    public function getDefaultConnectionName()
    {
        $this->default = $default;
    }

    /**
     * getConnection
     *
     * @param mixed $alias
     *
     * @return Connection
     */
    public function getConnection($alias = null)
    {
        $alias = $alias ?: $this->getDefaultConnectionName();

        return isset($this->connections[$alias]) ? $this->container->get($this->connections[$alias]) : null;
    }

    /**
     * getConnnections
     *
     * @return array
     */
    public function getConnnections()
    {
        $connections = [];

        foreach ($this->connections as $alias => $id) {
            $connections[$alias] = $this->container->get($id);
        }

        return $connections;
    }

    /**
     * hasConnection
     *
     * @param string $alias
     *
     * @return bool
     */
    public function hasConnection($alias)
    {
        return isset($this->connections[$alias]);
    }

    /**
     * get
     *
     * @param mixed $alias
     *
     * @return void
     */
    public function get($alias = null)
    {
        $key = $alias ?: $this->default;

        if (!isset($this->connections[$key])) {
            throw new \InvalidArgumentException('No connection selected or no default connection set.');
        }

        return $this->connections[$key]->createQueryBuilder();
    }

}
