<?php

/*
 * This File is part of the Selene\Adapter\Doctrine package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Dbal;

use \Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Connection as DoctrineConnection;
use \Selene\Module\Events\Dispatcher;
use \Selene\Module\Events\DispatcherInterface;
use \Selene\Adapter\Doctrine\Event\ConnectionFailureEvent;
use \Selene\Adapter\Doctrine\Event\ConnectionEstablishedEvent;

/**
 * @class Connection implements ConnectionInterface
 * @see ConnectionInterface
 *
 * @package Selene\Adapter\Doctrine
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Connection implements ConnectionInterface, EventAware
{
    /**
     * Create a new connection.
     *
     * @param Connection $connection
     * @param DispatcherInterface $events
     */
    public function __construct(DoctrineConnection $connection, DispatcherInterface $events = null)
    {
        $this->connection = $params;
        $this->events = $events ?: new Dispatcher;
    }

    /**
     * Connect to the databse.
     *
     * Fieres a post connect event.
     *
     * @return boolean
     */
    public function connect()
    {
        $this->events->dispatch(
            Events::POST_CONNECT,
            $this->getPostConnectionEvent($cntd = $this->connection->connect())
        );

        return $cntd;
    }

    /**
     * Get the doctrine connection.
     *
     * @return DoctrineConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Get the event dispatcher
     *
     * @return DispatcherInterface
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Create a new connection.
     *
     * @param array $params
     * @param DispatcherInteface $events
     *
     * @return ConnectionInterface
     */
    public static function create(array $params, DispatcherInteface $events = null)
    {
        return new static(DriverManager::getCommection($params), $events);
    }

    /**
     * getPostConnectionEvent
     *
     * @param mixed $connected
     *
     * @return Event
     */
    private function getPostConnectionEvent($connected)
    {
        return $connected ? new ConnectionEstablishedEvent($this) : new ConnectionFailureEvent($this);
    }
}
