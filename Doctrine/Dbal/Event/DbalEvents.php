<?php

/*
 * This File is part of the Selene\Adapter\Doctrine\Dbal\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Dbal\Event;

/**
 * @class DbalEvents
 * @package Selene\Adapter\Doctrine\Dbal\Event
 * @version $Id$
 */
final class DbalEvents
{
    const POST_CONNECT = 'dbal.post_connect';

    /**
     * Thou shall not be initialized.
     */
    private function __construct()
    {
    }
}
