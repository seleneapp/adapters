<?php

/*
 * This File is part of the Selene\Adapter\Doctrine\Dbal\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Dbal\Event;

/**
 * @class ConnectionEstablishedEvent
 * @package Selene\Adapter\Doctrine\Dbal\Event
 * @version $Id$
 */
class ConnectionEstablishedEvent extends ConnectionEvent
{
}
