<?php

/*
 * This File is part of the Selene\Adapter\Doctrine\Dbal\Event package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Dbal\Event;

use \Selene\Adapter\Doctrine\Dbal\ConnectionInterface;

/**
 * @abstract class ConnectionEvent extends Event
 * @see Event
 * @abstract
 *
 * @package Selene\Adapter\Doctrine
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
abstract class ConnectionEvent extends Event
{
    /**
     * connection
     *
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * getConnection
     *
     * @return ConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }
}
