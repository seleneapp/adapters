<?php

/**
 * This File is part of the Selene\Adapter\Doctrine\Orm package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Doctrine\Orm;

use \Doctrine\ORM\ORMException;
use \Doctrine\ORM\EntityManager;
use \Doctrine\Common\Persistence\AbstractManagerRegistry;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\ContainerAwareInterface;
use \Selene\Module\DI\Traits\ContainerAwareTrait;

/**
 * @class EntityManagerProvider
 * @package Selene\Adapter\Doctrine\Orm
 * @version $Id$
 */
class EntityManagerProvider extends AbstractManagerRegistry implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * services
     *
     * @var array
     */
    protected $services;

    public function __construct(ContainerInterface $container, array $connections, array $managers, $defaultConnection, $defaultManager)
    {
        $this->container = $container;

        parent::__construct(
            'ORM',
            $connections,
            $managers,
            $defaultConnection,
            $defaultManager,
            'Doctrine\ORM\Proxy\Proxy'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getService($name)
    {
        if (!isset($this->services[$name])) {
            return $this->services[$name] = $this->container->get($name);
        }

        return $this->services[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function resetService($name)
    {
        $this->services[$name] = null;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliasNamespace($namespaceAlias)
    {
        foreach ($k = array_keys($this->getManagers()) as $name) {

            $conf = $this->getManager($name)->getConfiguration();

            foreach ($conf->getEntityNamespaces() as $alias => $namespace) {
                if (0 === strcmp($alias, $namespaceAlias)) {
                    return $conf->getEntityNamespace($alias);
                }
            }
        }


        throw ORMException::unknownEntityNamespace($namespaceAlias);
    }
}
