<?php

/*
 * This File is part of the Selene\Adapter\Translation package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Translation;

use \Symfony\Component\Translation\TranslatorInterface;

/**
 * @class MessageFinder
 * @package Selene\Adapter\Translation
 * @version $Id$
 */
class MessageLoader implements MessageLoaderInterface
{
    /**
     * Constructor.
     *
     * @param array $paths
     * @param string $baseDir
     */
    public function __construct(TranslatorInterface $translator, array $paths, $baseDir = 'lang')
    {
        $this->paths = $paths;
        $this->baseDir = $baseDir;
        $this->translator = $translator;
    }

    /**
     * getTranslator
     *
     * @return TranslatorInterface
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * loadResource
     *
     * @param mixed $format
     * @param mixed $file
     * @param mixed $locale
     * @param mixed $domain
     *
     * @return void
     */
    public function loadResource($format, $file, $locale, $domain)
    {
        $this->translator->addResource($format, $file, $locale, $domain);
    }

    /**
     * find
     *
     * @return array
     */
    public function load()
    {
        foreach ($this->paths as $path) {
            $this->locateAndLoad($path);
        }
    }

    /**
     * findInPath
     *
     * @param mixed $path
     * @param array $resources
     *
     * @return void
     */
    protected function locateAndLoad($path)
    {
        $files = glob($path.'/'.$this->baseDir.'/*/*.{mo,po,php}', GLOB_BRACE) ?: [];

        $regexp = '~'.$this->baseDir.'\/(.+)\/(.+)\.(.+)$~';

        foreach ($files as $file) {
            if (preg_match($regexp, $file, $matches)) {

                list(, $locale, $domain, $format) = $matches;

                if ($format == 'php') {
                    $format = 'array';
                    $file = require($file);
                }

                $this->loadResource($format, $file, $locale, $domain);
                $this->loadResource($format, $file, substr($locale, 0, 2), $domain);
            }
        }
    }
}
