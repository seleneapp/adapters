<?php

/**
 * This File is part of the Selene\Adapter\Translation\Templating\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Translation\Twig;

use \Twig_Extension as TwigExtension;
use \Symfony\Component\Translation\TranslatorInterface;

/**
 * @class GetTextExtension
 * @package Selene\Adapter\Translation\Templating\Twig
 * @version $Id$
 */
class GetTextExtension extends TwigExtension
{
    /**
     * translator
     *
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Constructor.
     *
     * @param Translator $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * getName
     *
     * @return string
     */
    public function getName()
    {
        return 'gettext';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                '__',
                function ($id, array $parames = [], $domain = 'messages', $locale = null) {
                    return $this->translator->trans($id, $parames, $domain, $locale);
                }
            ),

            new \Twig_SimpleFunction(
                '_c',
                function ($id, $number, array $parames = [], $domain = 'messages', $locale = null) {
                    return $this->translator->transChoice($id, $number, $parames, $domain, $locale);
                }
            )
        ];
    }
}
