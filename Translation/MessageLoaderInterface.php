<?php

/*
 * This File is part of the Selene\Adapter\Translation package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Translation;

/**
 * @class MessageLoaderInterface
 * @package Selene\Adapter\Translation
 * @version $Id$
 */
interface MessageLoaderInterface
{
    public function getTranslator();

    public function load();

    public function loadResource($format, $file, $locale, $domain);
}
