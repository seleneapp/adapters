<?php

/**
 * This File is part of the Selene\Adapter\Translation package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Translation;

use \Symfony\Component\Translation\Translator;
use \Symfony\Component\Translation\Loader\ArrayLoader;
use \Symfony\Component\Translation\Loader\MoFileLoader;
use \Symfony\Component\Translation\Loader\PoFileLoader;

/**
 * @class TranslatorFactory
 *
 * @package Selene\Adapter\Translation
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class TranslatorFactory
{
    /**
     * make
     *
     * @param array $config
     *
     * @return Translator
     */
    public static function make(array $locales, array $loaders, $class = null)
    {
        if (null === $class) {
            $trans = new Translator($locales['default']);
        } else {
            $trans = new $class($locales['default']);
        }

        foreach ($loaders as $type => $loader) {
            $trans->addLoader($type, new $loader);
        }

        if (isset($locales['fallback'])) {
            $trans->setFallbackLocales((array)$locales['fallback']);
        }

        return $trans;
    }

    /**
     * This class may not be constructed.
     */
    private function __construct()
    {
    }
}
