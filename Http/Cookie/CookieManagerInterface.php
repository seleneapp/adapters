<?php

/*
 * This File is part of the Selene\Adapter\Http\Cookie package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Cookie;

use \Symfony\Component\HttpFoundation\Cookie;

/**
 * @class CookieManagerInterface
 *
 * @package Selene\Adapter\Http\Cookie
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface CookieManagerInterface
{
    /**
     * add
     *
     * @param Cookie $cookie
     *
     * @return void
     */
    public function add(Cookie $cookie);

    /**
     * has
     *
     * @param string $cookie
     *
     * @return boolean
     */
    public function has($cookie);

    /**
     * remove
     *
     * @param string $cookie
     *
     * @return void
     */
    public function remove($cookie);

    /**
     * flush
     *
     * @return array
     */
    public function flush();

    /**
     * getCookies
     *
     * @return array
     */
    public function getCookies();
}
