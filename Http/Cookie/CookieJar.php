<?php

/*
 * This File is part of the Selene\Adapter\Http\Cookie package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Cookie;

use \Symfony\Component\HttpFoundation\Cookie;
use \Selene\Module\Common\Data\AttributesTrait as Attributes;

/**
 * @class CookieJar
 *
 * @package Selene\Adapter\Http\Cookie
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class CookieJar
{
    use Attributes {
        Attributes::set as private setVal;
        Attributes::get as private getVal;
        Attributes::initialize as private init;
    }

    public function __construct(array $cookies = [])
    {
        $this->set($cookies);
    }

    /**
     * add
     *
     * @param Cookie $cookie
     *
     * @return void
     */
    public function add(Cookie $cookie)
    {
        $this->setVal($cookie->getName(), $cookie);
    }

    /**
     * get
     *
     * @param Cookie $cookie
     *
     * @return Cookie|null
     */
    public function get($cookie)
    {
        return $this->getVal($cookie, null);
    }

    /**
     * set
     *
     * @param array $cookies
     *
     * @return void
     */
    public function set(array $cookies)
    {
        $this->cookies = [];

        foreach ($cookies as $cookie) {
            $this->add($cookie);
        }
    }
}
