<?php

/*
 * This File is part of the Selene\Adapter\Http\Cookie package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Cookie;

use \Symfony\Component\HttpFoundation\Request;
use \Selene\Adapter\Kernel\StackedKernelTrait;
use \Selene\Adapter\Kernel\StackedKernelInterface;

/**
 * @class CookieMiddleware
 *
 * @package Selene\Adapter\Http\Cookie
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class CookieMiddleware implements StackedKernelInterface
{
    use StackedKernelTrait;

    /**
     * Constructor
     *
     * @param StorageInterface $store
     */
    public function __construct(CookieManager $manager, $priority = 110)
    {
        $this->manager = $manager;
        $this->priority = $priority;
    }

    /**
     * handle
     *
     * @param Request $request
     * @param string $type
     * @param boolean $catch
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {

        $this->manager->setFromRequest($request);

        $response = $this->getKernel()->handle($request, $type, $catch);

        $this->manager->writeToResponse($response);

        return $response;
    }
}
