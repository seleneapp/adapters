<?php

/*
 * This File is part of the Selene\Adapter\Http\Cookie package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Cookie;

use \Selene\Module\Common\IOPassThrough;
use \Selene\Module\Common\IOProxyInterface;
use \Symfony\Component\HttpFoundation\Cookie;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @class CookieManager
 *
 * @package Selene\Adapter\Http\Cookie
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class CookieManager implements CookieManagerInterface
{
    private $io;
    private $cookies;

    /**
     * Constructor.
     *
     * @param IOProxyInterface $io
     *
     * @return void
     */
    public function __construct(IOProxyInterface $io = null)
    {
        $this->io = $io ?: new IOPassThrough;
        $this->cookies = new CookieJar;
    }

    /**
     * add
     *
     * @param Cookie $cookie
     *
     * @return void
     */
    public function add(Cookie $cookie)
    {
        $this->cookies->add($cookie);
    }

    /**
     * has
     *
     * @param mixed $key
     *
     * @return boolean
     */
    public function has($key)
    {
        $this->cookies->has($key);
    }

    /**
     * remove
     *
     * @param mixed $key
     *
     * @return void
     */
    public function remove($key)
    {
        $this->cookies->remove($key);
    }

    /**
     * flush
     *
     * @return void
     */
    public function flush()
    {
        $cookies = $this->cookies->all();

        $this->cookies->set([]);

        return $cookies ?: [];
    }

    /**
     * getCookies
     *
     * @return void
     */
    public function getCookies()
    {
        return $this->cookies->all();
    }

    /**
     * readFromRequest
     *
     * @param Request $request
     *
     * @return void
     */
    public function setFromRequest(Request $request)
    {
        foreach ($request->cookies as $key => $cookie) {
            $request->cookies->set($key, $this->readValue($cookie));
        }
    }

    /**
     * writeToResponse
     *
     * @param Response $response
     *
     * @return void
     */
    public function writeToResponse(Response $response)
    {
        foreach (array_merge($response->headers->getCookies(), $this->flush()) as $cookie) {

            $cookie = $this->doDuplicate($cookie, $this->writeValue($cookie));
            $response->headers->setCookie($cookie);
        }
    }

    /**
     * newCookie
     *
     * @param mixed $name
     * @param mixed $value
     * @param int $expiry expirey time in minutes
     * @param mixed $path
     * @param mixed $domain
     * @param mixed $secure
     * @param mixed $httpOnly
     *
     * @return void
     */
    public function create($name, $value, $expire = 0, $path = null, $domain = null, $secure = false, $httpOnly = true)
    {
        $expire = time() + ($expire * 60);

        return new Cookie($name, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    /**
     * createForever
     *
     * @param mixed $name
     * @param mixed $value
     * @param mixed $path
     * @param mixed $domain
     * @param mixed $secure
     * @param mixed $httpOnly
     *
     * @return void
     */
    public function createForever($name, $value, $path = null, $domain = null, $secure = false, $httpOnly = true)
    {
        $expire = (strtotime('2037-12-31') / 60);

        return $this->create($name, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    /**
     * invalidate
     *
     * @param Cookie $cookie
     *
     * @return Cookie
     */
    public function invalidate(Cookie $cookie)
    {
        return $this->doDuplicate($cookie, null, -2628000);
    }

    /**
     * duplicate
     *
     * @param Cookie $cookie
     *
     * @return void
     */
    public function duplicate(Cookie $cookie)
    {
        return $this->doDuplicate($cookie);
    }

    /**
     * doDuplicate
     *
     * @param Cookie $cookie
     * @param mixed $expires
     *
     * @return Cookie
     */
    protected function doDuplicate(Cookie $cookie, $value = null, $expires = null)
    {
        return $this->create(
            $cookie->getName(),
            null !== $value ? $value: $cookie->getValue(),
            null !== $expires ? $expires : $cookie->getExpiresTime(),
            $cookie->getPath(),
            $cookie->getDomain(),
            $cookie->isSecure(),
            $cookie->isHttpOnly()
        );
    }

    /**
     * readValue
     *
     * @param mixed $value
     *
     * @return void
     */
    protected function readValue($value)
    {
        try {
            return $this->io->out($value);
        } catch (\Exception $e) {
        }

        return null;
    }

    /**
     * writeValue
     *
     * @param mixed $value
     *
     * @return void
     */
    protected function writeValue(Cookie $cookie)
    {
        return $this->io->in($cookie->getValue());
    }


    /**
     * getCookieClass
     *
     *
     * @return void
     */
    protected function getCookieClass()
    {
        return 'Symfony\Component\HttpFoundation\Cookie';
    }
}
