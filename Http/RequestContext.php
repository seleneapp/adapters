<?php

/**
 * This File is part of the Selene\Adapter\Http package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http;

/**
 * @class RequestContext
 * @package Selene\Adapter\Http
 * @version $Id$
 */
class RequestContext
{
    private $base;
    private $uri;
    private $query;
    private $method;
    private $host;
    private $port;
    private $scheme;

    public function __construct(
        $base = '',
        $uri = '/',
        $query = '',
        $method = 'GET',
        $host = 'localhost',
        $port = 80,
        $scheme = 'http'
    ) {
        $this->setRequest($request);
    }

    /**
     * fromRequest
     *
     * @param Request $request
     *
     * @return RequestContext
     */
    public static function fromRequest(Request $request)
    {
        return new static(
            $request->getBaseUrl(),
            $request->getPathInfo(),
            $request->getMethod(),
            $request->server->get('QUERY_STRING'),
            $request->getHost(),
            $request->getPort(),
            $request->getScheme()
        );
    }

    /**
     * getUri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * getMethod
     *
     * @return void
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * getQueryString
     *
     * @return string
     */
    public function getQueryString()
    {
        return $this->query;
    }

    /**
     * getQuery
     *
     * @return array
     */
    public function getQuery()
    {
        $query = [];

        parse_str($this->query, $query);

        return $query;
    }

    /**
     * getHost
     *
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * getBasePath
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->base;
    }

    /**
     * getPort
     *
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }
}
