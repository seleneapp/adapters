<?php

/**
 * This File is part of the Selene\Module\Net package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http;

use \SplStack;
use \Countable;
use \Symfony\Component\HttpFoundation\Request as SymfonyRequest;

/**
 * @class RequestStack implements StackInterface
 * @see StackInterface
 *
 * @package Selene\Module\Net
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class RequestStack implements RequestStackInterface, Countable
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->stack = new SplStack;
    }

    /**
     * getCurrent
     *
     * @return Request|null
     */
    public function getCurrent()
    {
        return $this->isEmpty() ? null : $this->stack->top();
    }

    /**
     * Return the previous request from the stack.
     *
     * If not previous request is available,
     * the current request is returned.
     *
     * @return Request|null
     */
    public function getPrevious()
    {
        if (0 === ($count = $this->stack->count()) || $count < 2) {
            return null;
        }
        return $this->stack[$count - ($count - 1)];
    }

    /**
     * Get the size of the stack
     *
     * @return int
     */
    public function count()
    {
        return $this->stack->count();
    }

    /**
     * removeLast
     *
     * @return Request
     */
    public function removeLast()
    {
        return $this->pop();
    }

    /**
     * isEmpty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return 0 === $this->stack->count();
    }

    /**
     * removeAll
     *
     * @return void
     */
    public function removeAll()
    {
        // stack->valid won't work strangely
        while ($this->count()) {
            $this->stack->pop();
        }
    }

    /**
     * push
     *
     * @param Request $request
     *
     * @return void
     */
    public function push(SymfonyRequest $request)
    {
        return $this->stack->push($request);
    }

    /**
     * pop
     *
     * @return Request
     */
    public function pop()
    {
        return $this->stack->pop();
    }

    /**
     * removeAllButFirst
     *
     * @return void
     */
    public function removeSubRequests()
    {
        $req = $this->stack->bottom();
        $this->removeAll();
        $this->stack->push($req);
    }

    /**
     * getMain
     *
     * @access public
     * @return Request|null
     */
    public function getMain()
    {
        if ($this->isEmpty()) {
            return null;
        }

        return $this->stack->bottom();
    }
}
