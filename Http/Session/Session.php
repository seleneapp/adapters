<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use \Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use \Symfony\Component\HttpFoundation\Session\SessionInterface;
use \Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeSessionHandler;
use \Symfony\Component\HttpFoundation\Session\SessionBagInterface;

/**
 * @class Session
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class Session implements SessionInterface
{
    private $storage;
    private $attributesName;
    private $flashesName;

    public function __construct(
        StorageInterface $storage = null,
        AttributeInterface $attributes = null,
        FlashBagInterface $flashes = null
    ) {
        $this->setStorage($storage);

        $this->registerBag($attributes = $attributes ?: new AttributeBag);
        $this->registerBag($flashes = $flashes ?: new FlashBag);

        $this->attributesName = $attributes->getName();
        $this->flashesName = $flashes->getName();
    }

    /**
     * setStorage
     *
     * @param StorageInterface $storage
     *
     * @return void
     */
    protected function setStorage(StorageInterface $storage = null)
    {
        $this->storage = $storage ?: new Storage(new NativeSessionHandler);
    }

    /**
     * {@inheritdoc}
     */
    public function start()
    {
        return $this->storage->start();
    }

    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        return $this->getAttributes()->has($name);
    }

    /**
     * {@inheritdoc}
     */
    public function get($name, $default = null)
    {
        return $this->getAttributes()->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function set($name, $value)
    {
        $this->getAttributes()->get($name);
    }

    protected function getAttribtues($name)
    {
        return $this->storage->getAttributes($this->attributesName);
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return $this->getAttributes()->all();
    }

    /**
     * {@inheritdoc}
     */
    public function replace(array $attributes)
    {
        $this->getAttributes()->replace($attributes);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        return $this->getAttributes()->remove($name);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->getAttributes()->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function isStarted()
    {
        return $this->storage->isStarted();
    }

    /**
     * Returns an iterator for attributes.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getAttributes()->all());
    }

    /**
     * Returns the number of attributes.
     *
     * @return int The number of attributes
     */
    public function count()
    {
        return count($this->getAttributes()->all());
    }

    /**
     * {@inheritdoc}
     */
    public function invalidate($lifetime = null)
    {
        $this->storage->clear();

        return $this->migrate(true, $lifetime);
    }

    /**
     * {@inheritdoc}
     */
    public function migrate($destroy = false, $lifetime = null)
    {
        return $this->storage->regenerate($destroy, $lifetime);
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        $this->storage->save();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->storage->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->storage->setId($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->storage->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->storage->setName($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadataBag()
    {
        return $this->storage->getMetaData();
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaData()
    {
        return $this->getMetadataBag();
    }

    /**
     * {@inheritdoc}
     */
    public function registerBag(SessionBagInterface $bag)
    {
        $this->storage->registerAttributes($bag);
    }

    /**
     * {@inheritdoc}
     */
    public function getBag($name)
    {
        return $this->storage->getAttributes($name);
    }

    /**
     * Gets the flashbag interface.
     *
     * @return FlashBagInterface
     */
    public function getFlashBag()
    {
        return $this->getAttributes($this->flashName);
    }
}
