<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Cookie package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Symfony\Component\HttpFoundation\Cookie;
use \Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @class Cookie
 *
 * @package Selene\Adapter\Http\Session\Cookie
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class SessionCookie extends Cookie
{
    public function __construct(SessionInterface $session, $expires = null, $path = null, $domain = null)
    {
        parent::__construct($session->getName(), $session->getId(), $expires ?: time() + 100, $path, $domain);
    }
}
