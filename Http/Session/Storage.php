<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Storage package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Rhumsaa\Uuid\Uuid;
use \SessionHandlerInterface;
use \Selene\Adapter\Http\Session\Data\MetaData;
use \Selene\Adapter\Http\Session\Data\SessionDataInterface;
use \Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use \Symfony\Component\HttpFoundation\Session\SessionBagInterface;

/**
 * @class Store
 *
 * @package Selene\Adapter\Http\Session\Storage
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class Storage implements StorageInterface
{
    protected $id;
    protected $closed;
    protected $started;
    protected $handler;
    protected $metadata;

    /**
     * Constructor.
     *
     * @param SessionHandlerInterface $handler
     * @param array $metadata
     * @param array $options
     */
    public function __construct(
        SessionHandlerInterface $handler,
        MetaData $metadata = null,
        $name = 'selene_session',
        $id = null
    ) {
        $this->closed  = false;
        $this->started = false;

        $this->handler = $handler;
        $this->setMetaData($metadata ?: new MetaData);
        $this->setName($name);
        $this->setId($id);
    }

    /**
     * setMetaData
     *
     * @param MetaData $meta
     *
     * @return void
     */
    public function setMetaData(MetaData $meta)
    {
        $this->metaData = $meta;
    }

    /**
     * getMetaData
     *
     * @return void
     */
    public function getMetaData()
    {
        return $this->metaData;
    }

    /**
     * registerAttributes
     *
     * @return void
     */
    public function registerAttributes(SessionBagInterface $attrs)
    {
        $this->attributes[$attrs->getName()] = $attrs;
    }

    /**
     * getAttributes
     *
     * @param mixed $name
     *
     * @return void
     */
    public function getAttributes($name)
    {
        if (!isset($this->attributes[$name])) {
            return;
        }

        // we need to ensure the session is loaded
        if ($this->isActive() && !$this->isStarted()) {
            $this->loadSession();
        } elseif ($this->isStarted()) {
            $this->start();
        }

        return $this->attributes[$name];
    }

    /**
     * Starts a session.
     *
     * @throws \RuntimeException a session was already started by php
     * @throws \RuntimeException if cookies are used an headers were already
     * sent.
     * @throws \RuntimeException if the session could not be started.
     *
     * @return boolean
     */
    public function start()
    {
        if ($this->isStarted()) {
            return false;
        }

        // throw an exception if cookies are uses to save a session
        if ($this->isActive()) {
            throw new \RuntimeException('Trying to start a new session, but a session was already started by PHP.');
        }

        // Cannot start a new session if cookies are used and headers have allready been sent.
        if (ini_get('session.use_cookie') && headers_sent($file, $line)) {
            throw new \RuntimeException(
                sprintf('Cannot start new session because headers have already being sent in %s at %s', $file, $line)
            );
        }

        //// If starting the session fails, just throw an exception here.
        //if (true !== session_start()) {
            //throw new \RuntimeException('Error while starting session');
        //}

        $this->loadSession();

        return true;
    }

    /**
     * isStarted
     *
     * @return boolean
     */
    public function isStarted()
    {
        return $this->started && !$this->closed;
    }

    /**
     * setId
     *
     * @param string $id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id ?: $this->generateId($this->getName());
    }

    /**
     * getId
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the session name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the session name
     *
     * @param string $name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * generateId
     *
     * @param mixed $name
     *
     * @return string
     */
    protected function generateId($name)
    {
        return $name .'_'. hash('sha1', Uuid::uuid4());
    }

    /**
     * Tells if the current session is active.
     *
     * @return boolean
     */
    public function isActive()
    {
        return PHP_SESSION_ACTIVE === session_status();
    }

    /**
     * Saves the current session.
     *
     * @return void
     */
    public function save()
    {
        $this->handler->write($this->getId(), serialize($this->mergeAttributes()));

        $this->closed = true;
        $this->started = !$this->closed;
    }

    /**
     * mergeAttributes
     *
     * @return void
     */
    protected function mergeAttributes()
    {
        $attrs = [];

        foreach ($this->attributesAll() as $attributes) {
            $attrs[$key = $attributes->getStorageKey()] = $attributes->all();
        }

        return $attrs;
    }

    /**
     * Clears the current session
     *
     * @return void
     */
    public function clear()
    {
        $_SESSION = [];
    }

    /**
     * Register a session save handler.
     *
     * @param SessionHandlerInterface $handler
     *
     * @return void
     */
    protected function registerHandler(SessionHandlerInterface $handler)
    {
        $this->handler = $handler;
    }

    /**
     * getDataFromHandler
     *
     * @return void
     */
    protected function getDataFromHandler()
    {
        $data = $this->getHandler()->read($this->getId());

        return unserialize($data);
    }

    /**
     * attributesAll
     *
     * @return array
     */
    protected function attributesAll()
    {
        return array_merge($this->attributes, [$this->metaData->getName() => $this->metaData]);
    }

    /**
     * Loads the session data from the session save handler.
     *
     * @return void
     */
    protected function loadSession()
    {
        $data = $this->getDataFromHandler();

        foreach ($this->attributesAll() as $attrs) {
            if (isset($data[$key = $attrs->getStorageKey()])) {
                $values = $data[$key];
            } else {
                $values = [];
            }

            $attrs->initialize($values);
        }

        $this->started = true;
        $this->closed = !$this->started;
    }

    /**
     * getHandler
     *
     * @return SessionHandlerInterface
     */
    protected function getHandler()
    {
        return $this->handler;
    }
}
