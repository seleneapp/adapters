<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Data package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Data;

/**
 * @class SessionCollectionInterface
 *
 * @package Selene\Adapter\Http\Session\Data
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface SessionDataInterface
{
    public function getName();

    public function getStorageKey();

    public function clear();

    public function initialize(array &$data);

    public function set($key, $value);

    public function get($key, $default = null);
}
