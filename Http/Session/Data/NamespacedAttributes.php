<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Data package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Data;

/**
 * @class NamespacedAttributes
 *
 * @package Selene\Adapter\Http\Session\Data
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class NamespacedAttributes extends Attributes
{
    private $separator;

    /**
     * Constructor.
     *
     * @param array $attributes
     * @param string $separator
     */
    public function __construct($name, $key, array $attributes = [], $separator = '.')
    {
        $this->separator = $separator;
        parent::__construct($name, $key, $attributes);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $this->setDefaultArray($this->attributes, $key, $value, $this->getSeparator());
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        return $this->getDefaultArray($this->attributes, $key, $default, $this->getSeparator());
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        return $this->unsetInArray($this->attributes, $key, $this->getSeparator());
    }

    /**
     * getSeparator
     *
     * @return string
     */
    protected function getSeparator()
    {
        return $this->separator;
    }
}
