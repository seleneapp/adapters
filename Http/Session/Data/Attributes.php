<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Data package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Data;

use \Selene\Module\Common\Data\AttributesTrait;
use \Symfony\Component\HttpFoundation\Session\SessionBagInterface;

/**
 * @class Attributes
 *
 * @package Selene\Adapter\Http\Session\Data
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
abstract class Attributes implements SessionBagInterface
{
    use AttributesTrait {
        AttributesTrait::initialize as private _init;
    }

    /**
     * name
     *
     * @var string
     */
    private $name;

    /**
     * storageKey
     *
     * @var string
     */
    private $storageKey;

    /**
     * Constructor.
     *
     * @param mixed $name
     * @param mixed $key
     * @param array $attributes
     */
    public function __construct($name, $key, array &$attributes = [])
    {
        $this->name = $name;
        $this->storageKey = $key;

        if (!empty($attributes)) {
            $this->initialize($attributes);
        } else {
            $this->attributes = [];
        }
    }

    /**
     * getName
     *
     *
     * @return void
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * getStorageKey
     *
     * @return string
     */
    public function getStorageKey()
    {
        return $this->storageKey;
    }

    /**
     * clear
     *
     * @return void
     */
    public function clear()
    {
        $data = [];
        $this->initialize($data);
    }

    /**
     * initialize
     *
     * @param array $data
     *
     * @return void
     */
    public function initialize(array &$data)
    {
        $this->attributes = &$data;
    }
}
