<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Data package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Data;

use \Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

/**
 * @class FlashData
 *
 * @package Selene\Adapter\Http\Session\Data
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class FlashData extends FlashBag
{
    public function __construct($key = '_selene_flashdata')
    {
        parent::__construct($key);
    }
}
