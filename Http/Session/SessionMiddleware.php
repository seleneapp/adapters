<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Session\SessionInterface;
use \Selene\Adapter\Kernel\StackedKernelTrait;
use \Selene\Adapter\Kernel\StackedKernelInterface;

/**
 * @class SessionHandler
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class SessionMiddleware implements StackedKernelInterface
{
    use StackedKernelTrait;

    private $session;

    /**
     * Constructor
     *
     * @param StorageInterface $store
     */
    public function __construct(SessionManagerInterface $manager, $priority = 100)
    {
        $this->manager = $manager;
        $this->priority = $priority;
    }

    /**
     * handle
     *
     * @param Request $request
     * @param string $type
     * @param boolean $catch
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $started = $this->startSessionFromRequest($request);

        $response = $this->getKernel()->handle($request, $type, $catch);

        if ($started) {
            $this->setSessionCookieOnResponse($response, $session = $request->getSession());
            $this->manager->stopSession($session);
        }

        return $response;
    }

    /**
     * startSessionFromRequest
     *
     * @param Request $request
     *
     * @return void
     */
    private function startSessionFromRequest(Request $request)
    {
        if (null === ($session = $request->getSession())) {
            $id = $request->cookies->get($this->manager->getSessionCookieKey());
            $request->setSession($session = $this->manager->createSession($id));
        }

        return $this->manager->startSession($session);
    }

    /**
     * setSessionCookieOnResponse
     *
     * @param Response $response
     * @param SessionInterface $session
     *
     * @return void
     */
    private function setSessionCookieOnResponse(Response $response, SessionInterface $session)
    {
        if (!$this->manager->isPersistent()) {
            return;
        }

        $response->headers->setCookie($this->manager->createCookie($session));
    }
}
