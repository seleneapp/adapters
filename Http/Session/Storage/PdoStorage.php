<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Storage package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Storage;

use \Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

/**
 * @class PdoStorage
 *
 * @package Selene\Adapter\Http\Session\Storage
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class PdoStorage implements SessionStorageInterface
{
    /**
     * Constructor.
     *
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }
}
