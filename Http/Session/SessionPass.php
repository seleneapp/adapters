<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Selene\Adapter\Kernel\StackedKernelTrait;
use \Selene\Adapter\Kernel\StackedKernelInterface;
use \Selene\Adapter\Http\RequestStackInterface;

/**
 * Passes sessions from a master request to subrequests.
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class SessionPass implements StackedKernelInterface
{
    use StackedKernelTrait;

    /**
     * stack
     *
     * @var RequestStackInterface
     */
    private $stack;

    /**
     * Constructor.
     *
     * @param RequestStackInterface $stack the request stack.
     * @param StackedKernelInterface $mdw the middleware that handles sessions
     */
    public function __construct(RequestStackInterface $stack, StackedKernelInterface $mdw)
    {
        $this->stack = $stack;
        $this->priority = $mdw->getPriority() + 100;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        if (self::SUB_REQUEST === $type) {
            $this->passSession($request);
        }

        return $this->getKernel()->handle($request, $type, $catch);
    }

    /**
     * Sets the session on the subrequest.
     *
     * If the subrequest already has a session, then this session will overridden.
     *
     * @param Request $request
     *
     * @return void
     */
    private function passSession(Request $request)
    {
        if (null !== ($session = $this->stack->getMain()->getSession())) {
            $request->setSession($session);
        }
    }
}
