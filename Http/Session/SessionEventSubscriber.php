<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Selene\Module\Events\SubscriberInterface;
use \Selene\Adapter\Kernel\Event\KernelEvents;
use \Selene\Adapter\Http\RequestStackInterface;
use \Symfony\Component\HttpKernel\HttpKernelInterface as Kernel;
use \Symfony\Component\HttpFoundation\Request;


/**
 * @class SessionEventSubstriber
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class SessionEventSubscriber implements SubscriberInterface
{
    /**
     * Constructor.
     *
     * @param SessionManagerInterface $manager
     */
    public function __construct(SessionManagerInterface $manager, RequestStackInterface $stack)
    {
        $this->sessions = [];

        $this->manager = $manager;
        $this->stack = $stack;
    }

    /**
     * passSession
     *
     * @param mixed $event
     *
     * @return void
     */
    public function passSession($event)
    {
        if (Kernel::SUB_REQUEST === $event->getRequestType()) {
            $this->doPassSession($event->getRequest());
        }
    }

    /**
     * startSession
     *
     * @param mixed $event
     *
     * @return void
     */
    public function startSession($event)
    {
        if (Kernel::MASTER_REQUEST === $event->getRequestType()) {
            $this->doStartSession($event->getRequest());
        }
    }

    /**
     * stopSession
     *
     * @param mixed $event
     *
     * @return void
     */
    public function stopSession($event)
    {
        $request = $event->getRequest();

        if (null === ($session = $request->getSession())) {
            return;
        }

        $this->manager->stopSession($session);

        if ($this->manager->isPersistent()) {
            $event->getResponse()->headers->setCookie($this->manager->createCookie($session));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscriptions()
    {
        return [
            KernelEvents::REQUEST => [
                ['passSession', 1001],
                ['startSession', 1000]
            ],
            KernelEvents::HANDLE_SHUTDOWN => [
                ['stopSession', 1001]
            ]
        ];
    }

    /**
     * doStopSession
     *
     * @param Request $session
     *
     * @return void
     */
    private function doStartSession(Request $request)
    {
        if (null === ($session = $request->getSession())) {
            $id = $request->cookies->get($this->manager->getSessionCookieKey());
            var_dump($id);
            $request->setSession($session = $this->manager->createSession($id));
        }

        return $this->manager->startSession($session);
    }

    /**
     * doStopSession
     *
     * @param Request $session
     *
     * @return void
     */
    private function doStopSession(Request $request)
    {
        if ($session = $request->getSession()) {
            $this->manager->stopSession($session);

            return $session;
        }
    }

    /**
     * doPassSession
     *
     * @param Request $session
     *
     * @return void
     */
    private function doPassSession(Request $session)
    {
        $master = $this->stack->getMain();

        if ($session = $master->getSession()) {
            $request->setSession($sesssion);
        }
    }
}
