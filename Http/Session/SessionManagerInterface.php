<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

use \Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @interface SessionManagerInterface
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface SessionManagerInterface
{
    /**
     * has
     *
     * @param mixed $sessId
     *
     * @return boolean
     */
    public function has($sessId);

    /**
     * get
     *
     * @param mixed $id
     *
     * @return SessionInterface|null
     */
    public function get($sessId);

    /**
     * hasSession
     *
     * @param SessionInterface $session
     *
     * @return boolean
     */
    public function hasSession(SessionInterface $session);

    /**
     * startSession
     *
     * @param SessionInterface $session
     *
     * @return boolean
     */
    public function startSession(SessionInterface $session);

    /**
     * stopSession
     *
     * @param SessionInterface $session
     *
     * @return boolean
     */
    public function stopSession(SessionInterface $session);

    /**
     * createCookie
     *
     * @param SessionInterface $session
     *
     * @return void
     */
    public function createCookie(SessionInterface $session);

    /**
     * createSession
     *
     * @param string $id
     *
     * @return SessionInterface
     */
    public function createSession($id = null);

    /**
     * isPersistnent
     *
     * @return boolean
     */
    public function isPersistent();

    /**
     * getSessionCookieKey
     *
     *
     * @return void
     */
    public function getSessionCookieKey();
}
