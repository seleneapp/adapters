<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

use \Selene\Module\Filesystem\Filesystem;
use \Selene\Module\Filesystem\FilesystemInterface;

/**
 * @class FilesystemSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class FilesystemSessionHandler extends AbstractSessionHandler
{
    private $fs;

    private $savePath;

    /**
     * Constructor
     *
     * @param FilesystemInterface $fs
     *
     * @return void
     */
    public function __construct($savePath, FilesystemInterface $fs = null)
    {
        $this->fs = $fs ?: new Filesystem;
        $this->savePath = $savePath;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        return $this->fs->exists($file = $this->savePath.DIRECTORY_SEPARATOR.$sessionId) ?
            gzuncompress($this->fs->getContents($file)) :
            '';
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        $this->fs->ensureDirectory($this->savePath);
        $this->fs->setContents($file = $this->savePath.DIRECTORY_SEPARATOR.$sessionId, gzcompress($data));

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        if ($this->fs->exists($file = $this->savePath.'/'.$sessionId)) {
            $this->fs->remove($file);
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function gc($maxlifetime)
    {
        foreach ($this->fs->directory($this->savePath)->files() as $file) {
            if ($this->fs->fileMtime($file) <= $maxlifetime) {
                $this->fs->remove($file);
            }
        }
    }
}
