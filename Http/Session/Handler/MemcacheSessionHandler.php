<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

use \Selene\Module\Cache\Driver\MemcacheConnection;

/**
 * @class MemcachedSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class MemcacheSessionHandler extends MemcachedSessionHandler
{
    /**
     * Constructor.
     *
     * @param MemcacheConnection $connection
     * @param int $ttl
     * @param string $prefix
     */
    public function __construct(MemcacheConnection $connection, $ttl = null, $prefix = '_selene.session')
    {
        $this->connection = $connection;
        AbstractSessionHandler::__construct($ttl, $prefix);
    }

    /**
     * close
     *
     * @return void
     */
    public function close()
    {
        return $this->connection->close();
    }
}
