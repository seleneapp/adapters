<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

use \Selene\Module\Cache\Driver\MemcachedConnection;

/**
 * @class MemcachedSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class MemcachedSessionHandler extends AbstractSessionHandler
{
    private $connected;
    protected $connection;

    public function __construct(MemcachedConnection $connection, $ttl = null, $prefix = '_selene.session')
    {
        $this->connection = $connection;
        parent::__construct($ttl, $prefix);
    }

    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        return $this->getCache()->get($this->getPrefixed($sessionId)) ?: '';
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        return $this->getCache()->set($this->getPrefixed($sessionId), $data, time() + $this->getTtl());
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        return $this->getCache()->delete($this->getPrefixed($sessionId));
    }

    /**
     * getCache
     *
     *
     * @return void
     */
    protected function getCache()
    {
        if (!$this->connected) {
            $this->connection->connect();
            $this->connected = true;
        }

        return $this->connection->getDriver();
    }
}
