<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

/**
 * @class ApcSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class ApcuSessionHandler extends ApcSessionHandler
{
    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        return apcu_fetch($this->getPrefixed($sessionId)) ?: '';
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        return apcu_store($this->getPrefixed($sessionId), $data, time() + $this->getTtl());
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        return apcu_delete($this->getPrefixed($sessionId));
    }
}
