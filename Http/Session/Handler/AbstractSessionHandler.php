<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

/**
 * @class AbstractSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
abstract class AbstractSessionHandler implements \SessionHandlerInterface
{
    private $ttl;
    private $prefix;

    public function __construct($ttl = null, $prefix = '_selene.session')
    {
        $this->ttl    = is_int($ttl) ? $ttl : 86400;
        $this->prefix = $prefix;
    }

    /**
     * {@inheritdoc}
     */
    public function open($savePath, $sessionName)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function gc($maxlifetime)
    {
        // not required here because apc will auto expire the records anyhow.
        return true;
    }

    /**
     * getTtl
     *
     * @return int
     */
    protected function getTtl()
    {
        return $this->ttl;
    }

    /**
     * getPrefix
     *
     * @return string
     */
    protected function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * getPrefix
     *
     * @return string
     */
    protected function getPrefixed($id)
    {
        return $this->prefix.$id;
    }
}
