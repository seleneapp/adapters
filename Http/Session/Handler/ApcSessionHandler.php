<?php

/*
 * This File is part of the Selene\Adapter\Http\Session\Handler package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session\Handler;

/**
 * @class ApcSessionHandler
 *
 * @package Selene\Adapter\Http\Session\Handler
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
class ApcSessionHandler extends AbstractSessionHandler
{
    /**
     * {@inheritdoc}
     */
    public function close()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function read($sessionId)
    {
        return apc_fetch($this->getPrefix().$sessionId) ?: '';
    }

    /**
     * {@inheritdoc}
     */
    public function write($sessionId, $data)
    {
        return apc_store($this->getPrefix().$sessionId, $data, time() + $this->getTtl());
    }

    /**
     * {@inheritdoc}
     */
    public function destroy($sessionId)
    {
        return apc_delete($this->getPrefix().$sessionId);
    }

    /**
     * {@inheritdoc}
     */
    public function gc($maxlifetime)
    {
        // not required here because apc will auto expire the records anyhow.
        return true;
    }
}
