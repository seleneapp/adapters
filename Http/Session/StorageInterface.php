<?php

/*
 * This File is part of the Selene\Adapter\Http\Session package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http\Session;

/**
 * @interface StorageInterface
 *
 * @package Selene\Adapter\Http\Session
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface StorageInterface
{
    public function start();

    public function isStarted();

    public function save();
}
