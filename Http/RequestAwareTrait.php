<?php

/**
 * This File is part of the Selene\Adapter\Http package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http;

use \Symfony\Component\HttpFoundation\Request;

/**
 * @trait RequestAwareTrait
 *
 * @package Selene\Adapter\Http
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
trait RequestAwareTrait
{
    /**
     * request
     *
     * @var Request
     */
    private $request;

    /**
     * setRequest
     *
     * @param Request $request
     *
     * @return void
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * getRequest
     *
     * @return Request|null
     */
    public function getRequest()
    {
        return $this->request;
    }
}
