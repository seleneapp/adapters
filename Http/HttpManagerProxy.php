<?php

/*
 * This File is part of the Selene\Adapter\Http package
 *
 * (c) iwyg <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @class HttpManagerProxy
 *
 * @package Selene\Adapter\Http
 * @version $Id$
 * @author iwyg <mail@thomas-appel.com>
 */
interface HttpManagerProxy
{
    public function isWrapper();

    public function fromRequest(Request $request);

    public function onResponse(Response $request);
}
