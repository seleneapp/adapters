<?php

/**
 * This File is part of the Selene\Adapter\Http package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Adapter\Http;

use \Symfony\Compoment\HttpFoundation\ParameterBag;
use \Symfony\Component\HttpFoundation\Request as SymfonyRequest;

/**
 * @class Request extends SymfonyRequest
 * @see SymfonyRequest
 *
 * @package Selene\Adapter\Http
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Request extends SymfonyRequest
{
    /**
     * getFromQuery
     *
     * @param string $param
     * @param mixed  $default
     * @param boolean $deep
     */
    public function getFromQuery($param, $default = null, $deep = false)
    {
        return $this->query->get($param, $default, $deep);
    }

    /**
     * getFromRequest
     *
     * @param string $param
     * @param mixed  $default
     * @param boolean $deep
     *
     * @return mixed
     */
    public function getFromRequest($param, $default = null, $deep = false)
    {
        return $this->request->get($param, $default, $deep);
    }

    /**
     * getAttribute
     *
     * @param string  $param
     * @param mixed   $default
     * @param boolean $deep
     *
     * @return mixed
     */
    public function getAttribute($attr, $default = null, $deep = false)
    {
        return $this->attributes->get($attr, $default, $deep);
    }

    /**
     * getInput
     *
     * @return array
     */
    public function getInput()
    {
        return new ParameterBag(array_merge($this->attributes->all(), $this->query->all(), $this->request->all()));
    }

    /**
     * isAjax
     *
     * @return boolean
     */
    public function isAjax()
    {
        return $this->isXmlHttpRequest();
    }
}
